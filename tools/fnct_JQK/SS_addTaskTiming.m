function SS_addTaskTiming(limits, lineWidth,lineStyle)

%     expInfo.durBlockOnset   = 5;	% duration of block onset
%     expInfo.durFixCue       = 1;	% duration of fixcross with cues
%     expInfo.durCue          = 0;	% duration of cue
%     expInfo.durPostCueFix   = 2;	% duration of postcue fixation interval
%     expInfo.durPres         = 3;	% duration of presentation
%     expInfo.durResp         = 2;	% duration of question
%     expInfo.durConf         = 0;	% duration of confidence
%     expInfo.durReward       = 3;	% duration of reward
%     expInfo.durITI          = 2;	% duration of ITI

% 1500 pre
% 1000*.5
% 2000*.5
% 3000*.5
% 2000*.5

%     timevec = cumsum([1500, 1000, 2000, 3000, 2000,1500].*.5);
% 
%     hold on; 
%     line([timevec(1) timevec(1)],limits,'LineWidth', 1, 'Color','k', 'LineStyle', '-.'); 
%     line([timevec(2) timevec(2)],limits,'LineWidth', 1, 'Color','k', 'LineStyle', '-.'); 
%     line([timevec(3) timevec(3)],limits,'LineWidth', 1, 'Color','r', 'LineStyle', '-.'); 
%     line([timevec(4) timevec(4)],limits,'LineWidth', 1, 'Color','r', 'LineStyle', '-.'); 
%     line([timevec(5) timevec(5)],limits,'LineWidth', 1, 'Color','k', 'LineStyle', '-.'); 
%     line([timevec(6) timevec(6)],limits,'LineWidth', 1, 'Color','w', 'LineStyle', '-.'); 
    
    timevec = [0 1000 3000 6066 8066 9566];

    hold on; 
    line([timevec(1) timevec(1)],limits,'LineWidth', lineWidth, 'Color','k', 'LineStyle', lineStyle); 
    line([timevec(2) timevec(2)],limits,'LineWidth', lineWidth, 'Color','k', 'LineStyle', lineStyle); 
    line([timevec(3) timevec(3)],limits,'LineWidth', lineWidth, 'Color','r', 'LineStyle', lineStyle); 
    line([timevec(4) timevec(4)],limits,'LineWidth', lineWidth, 'Color','r', 'LineStyle', lineStyle); 
    line([timevec(5) timevec(5)],limits,'LineWidth', lineWidth, 'Color','k', 'LineStyle', lineStyle); 
    line([timevec(6) timevec(6)],limits,'LineWidth', lineWidth, 'Color','w', 'LineStyle', lineStyle); 
    
end