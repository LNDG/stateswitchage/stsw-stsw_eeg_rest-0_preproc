function indx = finduniquerows(X)

% taken from:
% https://github.com/fieldtrip/fieldtrip/blob/master/private/mesh2edge.m
% Copyright (C) 2013-2015, Robert Oostenveld
% This file is part of FieldTrip, see http://www.fieldtriptoolbox.org

[X, indx] = sortrows(X);
sel  = any(diff([X(1,:)-1; X],1),2);
indx = indx(sel);