%% define root path

pn.root = '//mpib07/EEG-Desktop2/'; 
% pn.root = '/home/mwb/smb4k/MPIB07/EEG-Desktop2/';
%pn.root = 'M:/';

%% "set path" for analyses

% EEG tools folder
pn.fun = [pn.root 'ComMemEEGTools/'];

% change directory
cd([pn.fun 'fnct_THG'])

% add function folders folders for analysis
THG_addpath_fnct_common(pn.fun,0)
THG_addpath_fnct_thg(pn.fun)
THG_addpath_fieldtrip(pn.fun)

addpath([pn.fun '/fnct_MWB/MWB_ArtfDetec/'],'-end');
addpath([pn.fun '/fnct_MWB/MWB_ArtfDetec/external/'],'-end');

%% define IDs for preprocessing

% general path
pn.gen = [pn.root 'MERLIN/D_data/'];

cd(pn.gen)

files = dir;
cnt = 1;
for j = 1:length(files)
    if length(files(j).name) == 4 && files(j).isdir == 1
        ID(cnt,1) = str2num(files(j).name);
        cnt = cnt + 1;
    end
end; clear j

%% define experimental BLOCKs for preprocessing

BLOCK = 'L';


%% loop IDs & BLOCKS
id = [22] % 1 5 8 16 22
b  = 1

%%  define folders

    % path: history (e.g., EEG marked segments)
    pn.hst = [pn.gen '/' num2str(ID(id)) '/history/'];

    % path: raw EEG data 
    pn.beh = [pn.gen '/' num2str(ID(id)) '/beh/'];

    % path: raw EEG data 
    pn.dat = [pn.gen '/' num2str(ID(id)) '/raw_eeg/'];

    % path: processed eeg data
    pn.eeg = [pn.gen '/' num2str(ID(id)) '/eeg/'];


% function THG__20140312(pn,ID,BLOCK,segment)

% pn = path structure requiring following fields: 
% .hst = history folder
% .beh = raw behavioral data folder
% .eeg = raw eeg data folder
% .out = processed behavioral data folder
%
% ID has to be numeric

% required sub-functions:
% - 

%% read config file
load([pn.hst num2str(ID(id)) '_config_' BLOCK '.mat'])

%% read eeg data
load([pn.eeg num2str(ID(id)) '_' BLOCK '_Rlm_Fhl_Ica_Seg_Art.mat'])

%% read behavioral data
load([pn.beh 'Merlin_' num2str(ID(id)) '.mat'])

%% define data segments / conditions

% eeg trials
trl = config.ArtDect.final2original;

for j = 1:size(trl,1)
    
    % get index
    for k = 1:size(indData,1)
        if indData{k,15}==trl(j,2)
            ind = k;
        end
    end; clear k
    
    % get memory performance
    trl(j,4) = indData{ind,4};
    
    % clear variables
    clear ind
    
end; clear j

%%  filter data

% define settings for reading & preprocessing
cfg.channel     = {'all'};
cfg.continuous  = 'no';

cfg.demean      = 'yes';

cfg.hpfilter    = 'yes';
cfg.hpfreq      = 2;
cfg.hpfiltord   = 4;
cfg.hpfilttype  = 'but';

cfg.lpfilter    = 'yes';
cfg.lpfreq      = 8;
cfg.lpfiltord   = 4;
cfg.lpfilttype  = 'but';

% read & preprocess data
data = ft_preprocessing(cfg,data);

% clear cfg
clear cfg

%% collect data

dat1 = zeros(size(data.trial{1}));
dat2 = zeros(size(data.trial{1}));

cnt1 = 1;
cnt2 = 1;

for k = 1:length(data.trial)
    
    k

    % hilbert transform
    tmp = abs(hilbert(data.trial{1}')');
    
    if trl(k,4) == 1
        
%         dat1 = dat1 + tmp;
        dat1_(cnt1,:,:) = tmp;
        cnt1 = cnt1 + 1;
        
    elseif trl(k,4) == 0
        
%         dat2 = dat2 + tmp;
        dat2_(cnt2,:,:) = tmp;
        cnt2 = cnt2 + 1;
        
    end
    
    % clear variables
    clear tmp
    data.trial(1) = [];
    
end; clear k

clear data

% t-test on raw dat
[h p ci stats] = ttest2(dat1_,dat2_);

figure; imagesc(squeeze(stats.tstat))

time = [-1000:4:4500];

chan = 12;
figure;  plot(time,squeeze(mean(dat1_(:,chan,:))))
hold on; plot(time,squeeze(mean(dat2_(:,chan,:))),'r')

chan = 48;
figure;  plot(time,squeeze(mean(dat1_(:,chan,:))))
hold on; plot(time,squeeze(mean(dat2_(:,chan,:))),'r')

[h p ci stats] = ttest2(sum(dat1_(:,:,1:251),3),sum(dat2_(:,:,1:251),3));
figure; plot(stats.tstat)
hold on; plot(h,'or')

[h p ci stats] = ttest2(sum(dat1_(:,:,251:1251),3),sum(dat2_(:,:,251:1251),3));
hold on; plot(stats.tstat,'k')
hold on; plot(h,'og')

