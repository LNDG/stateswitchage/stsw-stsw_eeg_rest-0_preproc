function [freq, power] = showfft(F_tim, sampT, f_norm, s_line, h_axe)

% [freq, power] = showfft(F_tim, sampT, f_norm, s_line, h_axe)
%
%	Returns the magnitude of the positive frequencies
%	of the FFT in power and the corresponding frequency values in freq
%	and displays the Fourier Power spectrum of F_tim.  
% sampT    sampling period of data, default = 0.04.
% f_norm   normlize power magnitude (1), default = 0 (no).
% s_line   string for line types, plot symbols and colors, default = 'r-'.
% h_axe    handle of axes to use, [] = current axis, default new figure

if nargin < 2 | isempty(sampT),        sampT = 0.04;
end
if nargin < 3 | isempty(f_norm),       f_norm = 0; 
end
if nargin < 4 | isempty(s_line),       s_line = 'r-';
end

n_tim = length(F_tim);
nyquist = 1/(sampT*2);
freq = linspace(0, nyquist, n_tim/2)';
F_tim = F_tim - min(F_tim);

power = fft(F_tim);
power = power(1:floor(n_tim/2)).*conj(power(1:floor(n_tim/2)))/n_tim;
power(1) = 0;

if f_norm==1,                             power = power/sum(power);
end

if nargout==0 | nargin>3
   if nargin<5,                           h_axe=figure;
   elseif ~isempty(h_axe) & h_axe>0,      axes(h_axe);
   end
   %semilogx(freq, 10*log10(power), s_line) % decibels
   if h_axe>0
      semilogx(freq, power, s_line) % dave took out a 10*
      xlabel('Frequency (Hz)') 
      ylabel('Power Spectrum Magnitude ');
      if f_norm==0,                       title('FFT Spectral Estimate')
      else                                title('Normalized Fourier Power Spectrum')
      end
      grid on
   end
end

