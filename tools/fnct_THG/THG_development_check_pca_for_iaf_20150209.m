%% THG_check_pca_for_iaf_20150209

files = dir(['K:\CogitoEEG_IAF\D_data\E_IAF\*1.mat'])

for id = 1:length(files)
    
    load(['K:\CogitoEEG_IAF\D_data\E_IAF\' files(id).name],'ec')
    
    iaf(id,:) = ec(1:60,1);
    ID(id,1)  = str2num(files(id).name(1:5))
    
end; clear id
    
% check NaNs
% figure; plot(sum(isnan(iaf)))
% figure; plot(sum(isnan(iaf')))

% get IDs w/o NaNs
ind = find(sum(isnan(iaf'))==0);
iaf = iaf(ind,:); 
ID  = ID(ind); clear ind

% PCA
[coeff,score,latent,tsquared,explained,mu] = pca(iaf)