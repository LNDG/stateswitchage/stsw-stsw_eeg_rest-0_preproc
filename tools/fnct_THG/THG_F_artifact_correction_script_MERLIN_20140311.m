%% F_automatic_artifact_correction_MERLIN_20140311

%% define root path

 pn.root = '//mpib07/EEG-Desktop2/'; 
% pn.root = '/home/mwb/smb4k/MPIB07/EEG-Desktop2/';
%pn.root = 'M:/';
%pn.root = '/home/mcs/mounts/eeg-desktop2/'

%% "set path" for analyses

% EEG tools folder
pn.fun = [pn.root 'ComMemEEGTools/'];

% change directory
cd([pn.fun 'fnct_THG'])

% add function folders folders for analysis
THG_addpath_fnct_common(pn.fun,0)
THG_addpath_fnct_thg(pn.fun)
THG_addpath_fieldtrip(pn.fun)

addpath([pn.fun '/fnct_MWB/MWB_ArtfDetec/'],'-end');
addpath([pn.fun '/fnct_MWB/MWB_ArtfDetec/external/'],'-end');

%% define IDs for preprocessing

% general path
pn.gen = [pn.root 'MERLIN/D_data/'];

cd(pn.gen)

files = dir;
cnt = 1;
for j = 1:length(files)
    if length(files(j).name) == 4 && files(j).isdir == 1
        ID(cnt,1) = str2num(files(j).name);
        cnt = cnt + 1;
    end
end; clear j

%% define experimental BLOCKs for preprocessing

BLOCK = {'L','R1','R2','R3'};

%%  define segment
segment = [-1000 4500];

%% loop IDs & BLOCKS

%% artifact detection and exclusion
for id = 74%:74%length(ID)
for b  = 1:3%:length(BLOCK)

%%  define folders

    % path: history (e.g., EEG marked segments)
    pn.hst = [pn.gen '/' num2str(ID(id)) '/history/'];

    % path: behav data 
    pn.beh = [pn.gen '/' num2str(ID(id)) '/beh/'];

    % path: raw EEG data 
    pn.dat = [pn.gen '/' num2str(ID(id)) '/raw_eeg/'];

    % path: processed eeg data
    pn.eeg = [pn.gen '/' num2str(ID(id)) '/eeg/'];

    try
        display(['processing ID ' num2str(ID(id)) ', condition ' BLOCK{b}])
        if ~exist([pn.gen '/' num2str(ID(id)) '/eeg/' num2str(ID(id)) '_' BLOCK{b} '_Rlm_Fhl_Ica_Seg_Art.mat'],'file')

            if strcmp(BLOCK{b},'L')
            
                THG_automatic_artifact_correction_20140311(pn,ID(id),BLOCK{b},segment)
                
            elseif strcmp(BLOCK{b},'R1') || strcmp(BLOCK{b},'R2') || strcmp(BLOCK{b},'R3')

                THG_automatic_artifact_correction_20140320(pn,ID(id),BLOCK{b},segment)
            
            end
            
        end
    end
    
end; clear b
end; clear id

%% check trigger
% id = '3106';
% load(['//mpib07/EEG-Desktop2/MERLIN\D_data\' id '\beh\Merlin_' id '.mat'])
% figure; 
% subplot(221)
%     scatter(cell2mat(indData(2:end,15)),cell2mat(indData(2:end,43)))
%     title( ['L ' num2str(corr(cell2mat(indData(2:end,15)),cell2mat(indData(2:end,43))))])
%  subplot(222)
%     scatter(cell2mat(indData(2:end,16)),cell2mat(indData(2:end,44)))
%     title( ['R1 ' num2str(corr(cell2mat(indData(2:end,16)),cell2mat(indData(2:end,44))))])
% subplot(223)
%     scatter(cell2mat(indData(2:end,17)),cell2mat(indData(2:end,45)))
%     title( ['R2 ' num2str(corr(cell2mat(indData(2:end,17)),cell2mat(indData(2:end,45))))])
   
