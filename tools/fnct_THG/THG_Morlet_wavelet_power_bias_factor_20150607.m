function CF_pow = THG_Morlet_wavelet_power_bias_factor_20150607(cfg)
%
% input:    cfg     - .F          = frequency resolution
%                   - .fsample    = sampling frequency
%                   - .wavelet    = specific wavelet implementation
%                   - .wavenumber = number of cycles
%
% output:   CF_pow  = power bias correction factor for frequencies 
%                     specified in cfg.F

% THG 07.06.2015

%%  power bias for 1, 10, 100 Hz sine waves (8 sec)

    % frequency range
    F = [1 10 100];
    
    % loop frequenies
    for f = 1:3
        
        % signal: F(f) Hz
        time   = [0:1/cfg.fsample:10+(10+cfg.wavenumber/F(f))*2];
        signal = sin(time*F(f)*2*pi); 

        % wavelet transform
        if strcmp(cfg.wavelet,'BOSC_tf');
            B = BOSC_tf(signal,F(f),cfg.fsample,cfg.wavenumber);
        end
        
        % power
        pad1 = ceil((10+1/cfg.fsample)*cfg.fsample + cfg.wavenumber/F(f)*cfg.fsample);
        pad2 = floor(20*cfg.fsample + cfg.wavenumber/F(f)*cfg.fsample);
        pow(f,1)  = mean(B(pad1:pad2));
        
        % clear variables
        clear time signal B pad*
        
    end; clear f
    
    % linear regression
    beta = polyfit(log10(F)',log10(pow),1); 

    % power correction factor
    CF_pow = 10.^(polyval(beta,log10(cfg.F)));  
    
    % clear variables
    clear F time signal B pad* pow beta

    
