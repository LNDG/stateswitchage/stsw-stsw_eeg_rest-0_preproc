function THG_automatic_artifact_correction_resting_20141218(pn,ID,BLOCK)

% pn = path structure requiring following fields: 
% .hst = history folder
% .beh = raw behavioral data folder
% .eeg = raw eeg data folder
% .out = processed behavioral data folder
%
% ID has to be numeric

% required sub-functions:
% - 

%%  get config structure

%% ------------------ ARTIFACT DETECTION - PREPARATION ----------------- %%
    % load config
    load([pn.hst num2str(ID) '_config_' BLOCK],'config')
       
%% get raw data

    % define reading & preprocessing parameters
    cfg.headerfile  = [pn.dat config.header_file];
    cfg.datafile    = [pn.dat config.data_file];
    cfg.trl         = config.pnts_data;
    
    cfg.channel     = {'all'};
    cfg.continuous  = 'yes';
    cfg.demean      = 'yes';

    cfg.reref       = 'yes';
    cfg.refchannel  = {'A1','A2'};
    cfg.implicitref = 'A2';

    cfg.hpfilter    = 'yes';
    cfg.hpfreq      = .2;
    cfg.hpfiltord   = 4;
    cfg.hpfilttype  = 'but';

    cfg.lpfilter    = 'yes';
    cfg.lpfreq      = 250;
    cfg.lpfiltord   = 4;
    cfg.lpfilttype  = 'but';

    % get data
    data = ft_preprocessing(cfg);

    % clear cfg structure
    clear cfg

%%  remove alpha

    % define reading & preprocessing parameters
    cfg.headerfile  = [pn.dat config.header_file];
    cfg.datafile    = [pn.dat config.data_file];
    cfg.trl         = config.pnts_data;

    cfg.channel     = {'all'};
    cfg.continuous  = 'yes';
    cfg.demean      = 'yes';

    cfg.reref       = 'yes';
    cfg.refchannel  = {'A1','A2'};
    cfg.implicitref = 'A2';

    cfg.hpfilter    = 'yes';
    cfg.hpfreq      = 5;
    cfg.hpfiltord   = 4;
    cfg.hpfilttype  = 'but';

    cfg.lpfilter    = 'yes';
    cfg.lpfreq      = 15;
    cfg.lpfiltord   = 4;
    cfg.lpfilttype  = 'but';

    % get data
    data2 = ft_preprocessing(cfg);

    % clear cfg structure
    clear cfg
    
    % subtract alpha
    data.trial{1} = data.trial{1} - data2.trial{1};
    
    % clear variable
    clear data2
    
%%  remove data at edges

    % define number of data points to remove (1 sec of data at beginning and end)
    cfg.rm = [5000 5000];

    % remove data points at beginning and end of segments
    data = cm_remove_edges_20140122(data,cfg);
    data = ft_checkdata(data,'feedback','yes');

    % clear variables
    clear cfg

%%  segmentation of data
    
    % adapt trial structure to fieldtrip data structure
    trl = [config.trl_ec; config.trl_eo];
    trl(:,1:2) = trl(:,1:2) - config.pnts_data(1) + 1 - 5000;
    cfg.trl = trl;

    % segmentation of data
    data = cm_segmentation_of_continuous_data_fieldtrip_20141217(data,cfg);    
    
    % update sampleinfo (trials relative to raw data...)
    trl = [config.trl_ec; config.trl_eo];
    data.sampleinfo = trl(:,1:2);
    
    % get number of trials
    n_trl = size(trl,1);
    
    % clear variables
    clear cfg
    
    %%  resampling before ICA [1000 Hz]

    % define settings for resampling
    cfg.resamplefs = 1000;
    cfg.detrend    = 'no';
    cfg.feedback   = 'no';
    cfg.trials     = 'all';

    % resample
    data = ft_resampledata(cfg,data);

    % clear variables
    clear cfg

%% ICA (from weights)

    % ica config
    cfg.method           = 'runica';
    cfg.channel          = {'all','-ECG','-A2'};
    cfg.trials           = 'all';
    cfg.numcomponent     = 'all';
    cfg.demean           = 'yes';
    cfg.runica.extended  = 1;

    % ICA solution
    cfg.unmixing     = config.ica1.unmixing;
    cfg.topolabel    = config.ica1.topolabel;

    % components
    comp = ft_componentanalysis(cfg,data);

    % clear cfg
    clear cfg data

%% remove components

    % get IC labels
    iclabels = config.ica1.iclabels.manual;

    % cfg for rejecting components (reject: blinks, eye movements, ecg, ref)
    cfg.component = sortrows([iclabels.bli; iclabels.mov; iclabels.hrt; iclabels.ref])';
    cfg.demean    = 'yes';

    % reject components
    data = ft_rejectcomponent(cfg,comp);

    % clear cfg
    clear cfg comp

%% remove eye & reference channels

    cfg.channel     = {'all','-IOL','-LHEOG','-RHEOG','-A1'};
    cfg.demean      = 'yes';

    % remove channels
    tmpdat = ft_preprocessing(cfg,data);

    % clear cfg & data variable
    clear cfg data

%% ------------------------- ARTIFACT DETECTION ------------------------ %%

    % open log file
    fid = fopen([pn.hst 'log_' num2str(ID) '_ArtCorr_' BLOCK '.txt'],'a');

    % write log
    fprintf(fid,['*********************  ' BLOCK '  *********************\n']);
    fprintf(fid,['function: THG_automatic_artifact_correction_20141217.m \n\n']);
    fprintf(fid,['eeg file = ' config.data_file '\n\n']);


%%  get artifact contaminated channels by kurtosis, low & high frequency artifacts

    cfg.criterion = 3;
    cfg.recursive = 'no';

    [index0 parm0 zval0] = THG_MWB_channel_x_epoch_artifacts_20140311(cfg,tmpdat);

    % write log
    tmp_log = '';
    for j = 1:length(index0.c)
        tmp_log = [tmp_log num2str(index0.c(j)) ' '];
    end; clear j
    tmp_log = [tmp_log(1:end-1) '\n'];
    fprintf(fid,'(1) automatic bad channel detection:\n');
    fprintf(fid,['MWB:          channel(s) ' tmp_log]);

    % clear cfg
    clear cfg tmp_log

%%  get artifact contaminated channels by FASTER

    cfg.criterion = 3;
    cfg.recursive = 'no';

    [index1 parm1 zval1] = THG_FASTER_1_channel_artifacts_20140302(cfg,tmpdat);

    % write log
    tmp_log = '';
    for j = 1:length(index1)
        tmp_log = [tmp_log num2str(index1(j)) ' '];
    end; clear j
    tmp_log = [tmp_log(1:end-1) '\n'];
    fprintf(fid,['FASTER:      channel(s) ' tmp_log]);

    % clear cfg
    clear cfg tmp_log

%%  interpolate artifact contaminated channels

    % collect bad channels
    badchan = unique([index0.c; index1]);

    fprintf(fid,['--> ' num2str(length(badchan)) ' channels interpolated\n\n']);

    cfg.method     = 'spline';
    cfg.badchannel = tmpdat.label(badchan);
    cfg.trials     = 'all';
    cfg.lambda     = 1e-5; 
    cfg.order      = 4; 
    cfg.elec       = config.elec;

    % interpolate
    tmpdat = ft_channelrepair(cfg,tmpdat);

    % clear cfg
    clear cfg

%%  get artifact contaminated epochs & exclude epochs
    % includes: - THG_MWB_channel_x_epoch_artifacts_20140311
    %           - THG_FASTER_2_epoch_artifacts_20140302

    [tmpdat index] = THG_automatic_artifact_correction_trials_20141217(tmpdat);

    % write log
    fprintf(fid,'(2) automatic recursive bad epoch detection:\n');
    fprintf(fid,['MWB & FASTER: ' num2str(n_trl-length(index)) ' bad epoch(s) detected\n']);


%%  get channel x epoch artifacts

    % cfg
    cfg.criterion = 3;
    cfg.recursive = 'yes';

    [index3 parm3 zval3] = THG_FASTER_4_channel_x_epoch_artifacts_20140302(cfg,tmpdat);

    % write log
    fprintf(fid,'(3) automatic single epoch/channel detection:\n');
    fprintf(fid,['FASTER:       ' num2str(length(index3)) ' channel(s)/trial(s) detected & interpolated\n\n']);

    % clear cfg
    clear cfg
    
%%  collect and save detected artifacts & artifact correction infos
    
    % include ArtDect.parameters

    % bad channels
    ArtDect.channels = badchan;

    % bad trials
    tmp  = zeros(n_trl,1); tmp(index,1) = 1;
    badtrl = find(tmp==0);
    ArtDect.trials  = badtrl; 
    clear tmp

    % bad single epoch(s)/channel(s) - after exclusion of bad epochs
    ArtDect.channels_x_trials = index3;

    % overview
    ind = [1:n_trl];
    ind(badtrl) = [];
    tmp = ones(length(tmpdat.label),n_trl);
    tmp(:,ind) = index3;
    tmp(badchan,:) = 1;
    ArtDect.channels_x_trials_all = tmp;
    clear tmp

    % save version
    ArtDect.version = '20141217';

    % add to config
    config.ArtDect = ArtDect;

    % save config
    save([pn.hst num2str(ID) '_config_' BLOCK '.mat'],'config')

%%  finalize log
    
    % log
    fprintf(fid,'Artifact detection completed.\n');
    fprintf(fid,['Information saved to: ' num2str(ID) '_config_' BLOCK '.mat\n\n']);
    fclose(fid);
    
    
    
    
    
