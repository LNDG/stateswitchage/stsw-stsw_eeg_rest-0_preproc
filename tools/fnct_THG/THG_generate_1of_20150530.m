function [signal parm] = THG_generate_1of_20150530(cfg)
%
% signal = THG_generate_1of_20150530(cfg)
% 
% generates a signal with 1/f characteristic
%
% input:    cfg.Fs      = sampling rate
%           cfg.Ls      = length of signal in seconds
%           cfg.a       = intercept of 1/f spectrum
%           cfg.b       = slope of 1/f spectrum
%           cfg.hpf     = high pass frequency cutoff
%           cfg.lpf     = low pass frequency cutoff
%           cfg.noise   = noise type added to spectrum 
%                         currently available: 'randn';
%           cfg.noise_r = noise SD as a fraction (r) of amplitude
%
% output:   signal      = signal with 1/f characteristic
%           parm.freq   = frequencies
%           parm.amp    = amplitudes w/o noise

% THG 30.05.2015


T    = 1/cfg.Fs;           % Sample time
L    = cfg.Ls*cfg.Fs;      % Length of signal
time = (0:L-1)*T;          % Time vector

% check for intercept and slope of 1/f spectrum are defined
% if not take mean values from Freeman et al. (2000)
if ~isfield(cfg,'a')
    cfg.a = .89;
end
if ~isfield(cfg,'b')
    cfg.b = -1.97;
end

% calculate frequency resolution & upper frequency = 1/2 Fs 
freq = 1/cfg.Ls:1/cfg.Ls:.5*cfg.Fs;
% generate amplitudes of frequencies [see Freeman et al. (2000)]
for j = 1:length(freq)
    amplitude(j) = exp(.5*cfg.a + .5*cfg.b*log(freq(j)));
end; clear j    

% keep frequency resolution and amplitude as parameters
parm.freq = freq;
parm.amp  = amplitude;

% "exclude" frequencies at lower end if high pass frquency cutoff is defined
% i.e. set amplitude at lower end to cutoff frequency amplitude
if isfield(cfg,'hpf')
    lower = find(freq>cfg.hpf,1);
    amplitude(1:lower-1) = amplitude(lower); clear lower
end

% "exclude" frequencies at upper end if low pass frquency cutoff is defined
% i.e. set amplitude at lower end to cutoff frequency amplitude
if isfield(cfg,'lpf')
    upper = find(freq>cfg.lpf,1);
    amplitude(upper:end) = amplitude(upper); clear upper
end

% add noise
if isfield(cfg,'noise')
for j = 1:length(freq)
    eval(['amplitude(j) = amplitude(j) + (cfg.noise_r .* amplitude(j) * ' cfg.noise '(1));']);
end; clear j
end

% initialize signal vector
signal = zeros(1,L);

% loop through frequencies
for j = 1:length(freq)
    
    % generate random phase
    t = (time + rand(1)/freq(j));
    
    % keep approximate phase at point 1
    phase(j) = (t(1) / (1/freq(j))) * 2*pi;
    
    % generate signal and add up
    signal = signal + sin(2*pi*freq(j)*t) * amplitude(j);
    
end; clear j

%% plot 
% figure; 
% subplot(221); plot(time,signal)
% xlabel('time [s]')
% ylabel('amplitude [\muV]')
% 
% % calculate fft
% Y = fft(signal,L)/L;
% f = Fs/2*linspace(0,1,L/2);
% 
% % Plot single-sided amplitude spectrum.
% subplot(222); plot(f(2:end),log(2*abs(Y(2:L/2)))) 
% title('single-sided amplitude spectrum of signal(t)')
% xlabel('frequency (Hz)')
% ylabel('log(|Y(f)|)')
% 
% subplot(223); plot(log(f(2:end)),log(2*abs(Y(2:L/2))))
% title('1/f of the single sided amplitude spectrum')
% xlabel('log(frequency)')
% ylabel('log(|Y(f)|)')

