% pop_readNEUROPRAX() - store the data from readNEUROPRAX in EEGLAB's
%                       EEG-structure
%
% Usage:
%   >>  [EEG, com] = pop_readNEUROPRAX(filename);
%
% Inputs:
%   filename - Filename of imported dataset
%    
% Outputs:
%   EEG  - structure according to eeglab standard
%   com  - command output
%
% See also:
%   readNEUROPRAX(), eegplugin_NEUROPRAX(), eeglab()
%
% Author:   Falk Schlegelmilch for the *.EEG Matlab reading functions. 
%           Sophia Wunder for the EEGLAB interface.
%
% Copyright (C) 2017 Sophia Wunder (sophia.wunder@neurocaregroup.com),
%           neuroConn GmbH, Ilmenau, Germany
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [EEG, com] = pop_readNEUROPRAX(filename)

% the command output is a hidden output that does not have to
% be described in the header

com = ''; % this initialization ensure that the function will return something
          % if the user press the cancel button            

% display help if not enough arguments
% ------------------------------------
if nargin < 1
	% ask user for file
	[name, filepath] = uigetfile('*.EEG', 'Choose an EEG file -- pop_sampleNP()'); 
    drawnow;
    disp(name);
	if name == 0 return; end;
	filename = [filepath name];
    
    disp(['Reading in ',filename,' ...']);
end;	

% -------------------------------------------------------------------------
% Initializing of EEG-structure
% -------------------------------------------------------------------------
EEG = struct('setname','',...
             'filename','',...
             'filepath','',...
             'pnts',0,...
             'nbchan',0,...
             'trials',0,...
             'srate',0,...
             'xmin',0,...
             'xmax',0,...
             'data',0,...
             'icawinv',[],...
             'icasphere',[],...
             'icaweights',[],...
             'icaact',[],...
             'event','',...
             'epoch','',...
             'chanlocs','',...
             'comments','',...
             'averef','',...
             'rt',[],...
             'eventdescription','',...
             'epochdescription','',...
             'specdata',[],...
             'specicaact',[],...
             'reject','',...
             'stats','',...
             'splinefile',[],...
             'ref','',...
             'history','',...
             'urevent','',...
             'times',0,...
             'saved','yes');

% -------------------------------------------------------------------------
% Storing the strukture elements
% -------------------------------------------------------------------------
[DAT,INFO,MARKER] = readNEUROPRAX( filename );  % Read in all data        
         
EEG.setname = strrep(name,'.EEG','');   % setname is filename
EEG.filename = name;
EEG.filepath = filepath;
EEG.pnts = INFO.N;      % number of data points
EEG.nbchan = INFO.K;    % number of channels
EEG.trials = 1;         % continous data                      
EEG.srate = INFO.fa;    % sampling rate
EEG.xmin = 0;           % time minimum
EEG.xmax = (EEG.pnts-1)/EEG.srate; % time maximum
EEG.data = double(ones(EEG.nbchan,EEG.pnts)); % raw data in double
EEG.data = DAT.data';   % columns represent channels
EEG.averef = 'no';      % no average reference
EEG.times = (0 : 1 : EEG.pnts-1)./EEG.srate; % time vector
% initilizing channel locations
EEG.chanlocs = struct('theta',0,'radius',0,'labels','',...
                      'sph_theta',0,'sph_phi',0,'sph_radius',0,...
                      'X',0,'Y',0,'Z',0);
for c=1:EEG.nbchan
    EEG.chanlocs(c).labels = INFO.channels{1,c}; % channel names
end

% find reference
% indRef = strfind(INFO.channeltypes,'REF_EEG');
% indRef = find(not(cellfun('isempty', indRef)));
% if isempty(indRef)
%     EEG.ref = 'unknown'; % if no reference defined
% else
%     EEG.ref = indRef; % reference channel
% end


% NEUROPRAX data are recorded unipolar
% they should always be re-referenced to the reference channel
% in order to improve the signal-to-noise ratio


% Initialize EEG.event structure
EEG.event = struct('type','Trigger','position','',...
                   'latency',0,'urevent',0);
% Read in events/markers               
I = find(~cellfun('isempty', MARKER.marker));
j=0;
for n=1:length(I)
    descr = MARKER.markernames{I(n),1};
    array = MARKER.marker{I(n),1};
    for i=1:length(array)
        EEG.event(j+i).type = descr;    % event name
        EEG.event(j+i).urevent = i;     % original event number
        EEG.event(j+i).latency = array(i); % latency in samples
        if EEG.event(j+i).latency == 0 % 0 als Index verboten
            EEG.event(j+i).latency = 1; % wird auf Indexnr. 1 gesetzt
        end
    end
    j=j+i;
end

EEG.urevent = EEG.event; % urevents initialized as events

% define channel names and locations
% initializing channel locations structure
EEG.chanlocs = struct('theta',0,'radius',0,'labels','',...
                      'sph_theta',0,'sph_phi',0,'sph_radius',0,...
                      'X',0,'Y',0,'Z',0);
                  
% initializing structrure to read from map
N = struct('Number',0,'labels','','theta',0,'radius',0,...
           'X',0,'Y',0,'Z',0,'sph_theta',0,'sph_phi',0,...
           'sph_radius',0);


% use 81-channel (10-10) map from samplelocations of eeglab as template
F= 'Standard-10-20-Cap81.ced'; % needs to be stored in NEUROPRAX-plugin folder
if exist(F,'file')<2
    disp(['Error load channel location map: File ',F,' not found. \n',...
        'Please check if it is stored in the folder "eegplugin_NEUROPRAXimport". \n']);
    return;
else
    [number] = textread(F,'%s');
    % read in 81 channel location data
    for c=1:length(number)/10 -1
        N(c).Number = int32(str2double(number{c*10+1,1}));
        N(c).labels = number{c*10+2,1};
        N(c).theta = str2double(number{c*10+3,1});
        N(c).radius = str2double(number{c*10+4,1});
        N(c).X = str2double(number{c*10+5,1});
        N(c).Y = str2double(number{c*10+6,1});
        N(c).Z = str2double(number{c*10+7,1});
        N(c).sph_theta = str2double(number{c*10+8,1});
        N(c).sph_phi = str2double(number{c*10+9,1});
        N(c).sph_radius = str2double(number{c*10+10,1});
    end
end

% define channel locations
IndChan = cell(EEG.nbchan,1);                  
for c=1:EEG.nbchan
    EEG.chanlocs(c).labels = INFO.channels{1,c};
    
    if strcmpi(EEG.chanlocs(c).labels,'REF_EEG') == 1
       % if REF_EEG: right mastoid by default 
        IndChan{c,1} = 0;
        EEG.chanlocs(c).theta = 101.3099;
        EEG.chanlocs(c).radius =0.5;
        EEG.chanlocs(c).X = -0.2;
        EEG.chanlocs(c).Y = -1;
        EEG.chanlocs(c).Z = 0;
        EEG.chanlocs(c).sph_theta = 101.3099;
        EEG.chanlocs(c).sph_radius = 0;
        EEG.chanlocs(c).sph_phi = 1.0198;
    
    else
        for cc=1:length(N)
%             disp([num2str(cc),': ',num2str(INFO.channels{1,c}),' - ',num2str(N(cc).labels)]);
            if strcmpi(N(cc).labels,INFO.channels{1,c}) == 1
               IndChan{c,1} = cc;
            end
        end

        if isempty(IndChan{c,1})
            % if no channel of 10-10 system (zB EMG1), then Pos=(0,0,0)
            IndChan{c,1} = 0;
            EEG.chanlocs(c).theta = 0;
            EEG.chanlocs(c).radius =0;
            EEG.chanlocs(c).X = 0;
            EEG.chanlocs(c).Y = 0;
            EEG.chanlocs(c).Z = 0;
            EEG.chanlocs(c).sph_theta = 0;
            EEG.chanlocs(c).sph_radius = 0;
            EEG.chanlocs(c).sph_phi = 0;
        else
            % if channel from 10-10 system, then use template
            EEG.chanlocs(c).theta = N(IndChan{c,1}).theta;
            EEG.chanlocs(c).radius = N(IndChan{c,1}).radius;
            EEG.chanlocs(c).X = N(IndChan{c,1}).X;
            EEG.chanlocs(c).Y = N(IndChan{c,1}).Y;
            EEG.chanlocs(c).Z = N(IndChan{c,1}).Z;
            EEG.chanlocs(c).sph_theta = N(IndChan{c,1}).sph_theta;
            EEG.chanlocs(c).sph_radius = N(IndChan{c,1}).sph_radius;
            EEG.chanlocs(c).sph_phi = N(IndChan{c,1}).sph_phi;
        end
    end
end

% create text file with these channel locations

fid = fopen([filepath,'loc_for_',name,'.ced'],'wt'); % overwriting if already exists
if fid<0
    disp(['Error load *.ced file: ',filepath,'loc_for_',...
        name,'.ced for saving channel locations not found. \n']);
    fclose(fid);
    return;
else
    fn = fieldnames(N);
    for k=1:length(fn)   
        fprintf(fid,'%s\t',fn{k,1});
    end

    for k=1:length(EEG.chanlocs)
        fprintf(fid,'\n%.4g\t',k);
        fprintf(fid,'%s\t',EEG.chanlocs(k).labels);
        fprintf(fid,'%.4f\t',EEG.chanlocs(k).theta);
        fprintf(fid,'%.4f\t',EEG.chanlocs(k).radius);
        fprintf(fid,'%.4f\t',EEG.chanlocs(k).X);
        fprintf(fid,'%.4f\t',EEG.chanlocs(k).Y);
        fprintf(fid,'%.4f\t',EEG.chanlocs(k).Z);
        fprintf(fid,'%.4f\t',EEG.chanlocs(k).sph_theta);
        fprintf(fid,'%.4f\t',EEG.chanlocs(k).sph_phi);
        fprintf(fid,'%.4f\t',EEG.chanlocs(k).sph_radius);
    end
    fclose(fid);
    disp(['Saving channel locations to: ',filepath,'loc_for_',name,'.ced']);
end


disp('Done');

% return the string command
% -------------------------

% com = sprintf('pop_sample( %s );', filename);


return;
