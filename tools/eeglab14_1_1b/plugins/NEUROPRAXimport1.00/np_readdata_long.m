function [np_data] = np_readdata_long (filename, idx_begin, data_length, option)
%
% function [np_data] = np_readdata_long (filename, idx_begin, data_length, option)
%
% np_readdata reads data from multiple NEURO PRAX data files (*.X*.EEG).
%
%
% Syntax:
%
%   [np_data] = np_readdata(filename,idx_begin,data_length,'samples');
%   [np_data] = np_readdata(filename,idx_begin,data_length,'time');
%
% Input data:
%
%   filename    -   the complete filename with path
%                   (e. g.  C:\Document...\20030716103637.EEG)
%   idx_begin   -   the start index of the data block to be read
%   data_length -   the length of the data block to be read
%   option      -   if option = 'samples':
%                       the data block starts at sample 'idx_begin' from the recording;
%                       data_length is the number of samples to be read
%                   if option = 'time':
%                       the data block starts at time 'idx_begin' from the recording;
%                       data_length is the number of seconds to be read
%
%                   To read all data use: idx_start = 0, data_length = inf, option =
%                   'samples'.
%
% Output data:
%
%   np_data     -   structure
%      np_data.data     -   data matrix of unipolar raw data
%                           dimension of the matrix: (NxK)
%                           N: number of samples
%                           K: number of channels (each column is one channel)
%      np_data.t        -   discrete time vector for the recording
%      np_data.DTRIG1   -   values of digital trigger 1 with logical output (if available)
%      np_data.DTRIG2   -   values of digital trigger 2 with logical output (if available)
%      np_data.DTRIG3   -   values of digital trigger 3 with logical output (if available)
%      np_data.DTRIG4   -   values of digital trigger 4 with logical output (if available)
%
% Version:  1.3. (2012-05-25)
%
% See also: np_readfileinfo, np_readmarker
%
% neuroConn GmbH
% Grenzhammer 10
% D-98693 Ilmenau
% Germany
% 24.05.2012

% Info's lesen
fileSeparator=filesep;
np_info=np_readfileinfo(filename,'NO_MINMAX');

% global N-Werte bestimmen
if (0==idx_begin) && (inf==data_length)
    Nstart_global=0;
    N = np_info.N;                 
else
    switch upper(option)
        case 'SAMPLES', 
            Nstart_global   = idx_begin;
            N               = data_length;
        case 'TIME',
            Nstart_global   = round(idx_begin*np_info.fa);
            N               = round(data_length*np_info.fa);
        otherwise,
            error ('Bad specification for ''option''');
    end
    if (Nstart_global<0)
        error ('idx_begin is to small.');
    end
    if ((Nstart_global+N-1)>(np_info.N-1))
        error('data_length is to big.');
    end
end
Nend_global=Nstart_global+N-1;

% f�r die gesplitteten Files die Grenzen einlesen
% Achtung: die Grenzen sind index-1 beginnend
r(1:np_info.SPLIT_Z)=struct('A',[],'B',[]);
r(1).A=1;
r(1).B=np_info.SPLIT_N(1);
for z=2:np_info.SPLIT_Z
    r(z).A=r(z-1).B+1;
    r(z).B=r(z).A+np_info.SPLIT_N(z)-1;
end
for z=1:np_info.SPLIT_Z
    r(z).A=r(z).A-1;
    r(z).B=r(z).B-1;
end

% jede *.X*.EEG Datei einzeln behandeln
np_data.data=single([]);
for z=1:np_info.SPLIT_Z

    % Fall R1
    if (Nstart_global>r(z).B) || (Nend_global<r(z).A)
        continue;
    end
    
    % Fall R2.a
    if (Nstart_global<=r(z).A) && (Nend_global<r(z).B)
        Nstart=0;
        N=(Nend_global-r(z).A+1);
    end
    
    % Fall R2.b
    if (Nstart_global<=r(z).A) && (Nend_global>=r(z).B)
        Nstart=0;
        N=r(z).B-r(z).A+1;
    end
    
    % Fall R2.c
    if (Nstart_global>r(z).A)
        Nstart=Nstart_global-r(z).A;
        if (r(z).B>Nend_global)
            N=Nend_global-Nstart_global+1;
        else
            N=r(z).B-Nstart_global+1;
        end
    end

    % -------------------------------------------------------------------------
    % Messadten einlesen
    %
    % sequentielles Datenformat in Datei (K-Kanalindex, N-Sampleindex):
    % x11 x21 x31 ... xK1 ...  
    % x12 x22 x32 ... xK2 ...
    % ...
    % x1(N-1) x2(N-1) ... xK(N-1) ...
    % x1N x2N x3N ... xKN
    %
    % einlesen in eine Matrix mit der Dimension: (KxN)
    % anschlie�end transponieren in eine Matrix der Dimension: (NxK)
    %
    % -------------------------------------------------------------------------
    fullfile(np_info.pathname,np_info.SPLIT_filename{z})
    fid=fopen(fullfile(np_info.pathname,np_info.SPLIT_filename{z}),'r');
    if fid==-1,
        error(['Unable to open file "' np_info.SPLIT_filename{z} '" . Error code: ' ferror(fid)]);
    end
    status=fseek(fid,np_info.SPLIT_fp_data(z)+Nstart*np_info.K*4,'bof');    % 4 Byte pro Sample
    if status~=0,
        fclose(fid);
        error('Unable to set filepointer to begin of data block.');
    end
    [data,count]=fread(fid,[np_info.K N],'float');      % lese Messdaten
    if count~=N*np_info.K,
        fclose(fid);
        error('Number of read samples unequal to product N*K.');
    end
    data=data';         % transponieren
    np_data.data=[np_data.data; single(data)];
    fclose(fid);
end

% Zeitvektor und DTRIG bereitstellen
np_data.t=single((Nstart_global:Nend_global)'./np_info.fa);
DTRIGchannel=find(strcmp(upper(np_info.channels),'DTRIG')==1.0);
if ~isempty(DTRIGchannel)
    DTRIG=np_data.data(:,DTRIGchannel);

    np_data.DTRIG1=single(not(bitget(double(DTRIG),1)));
    np_data.DTRIG1_posTrig=find(diff(np_data.DTRIG1)==+1.0);
    np_data.DTRIG1_negTrig=find(diff(np_data.DTRIG1)==-1.0);

    np_data.DTRIG2=single(not(bitget(double(DTRIG),2)));
    np_data.DTRIG2_posTrig=find(diff(np_data.DTRIG2)==+1.0);
    np_data.DTRIG2_negTrig=find(diff(np_data.DTRIG2)==-1.0);

    np_data.DTRIG3=single(not(bitget(double(DTRIG),3)));
    np_data.DTRIG3_posTrig=find(diff(np_data.DTRIG3)==+1.0);
    np_data.DTRIG3_negTrig=find(diff(np_data.DTRIG3)==-1.0);

    np_data.DTRIG4=single(not(bitget(double(DTRIG),4)));
    np_data.DTRIG4_posTrig=find(diff(np_data.DTRIG4)==+1.0);
    np_data.DTRIG4_negTrig=find(diff(np_data.DTRIG4)==-1.0);
end