% readNEUROPRAX(filename) - read in information, data and events
%                           of the *.EEG data recorded with neuroConn's
%                           NEURO PRAX system
%
% Usage:
%   >>  [NP_data, NP_info, NP_marker] = readNEUROPRAX(filename);
%
% Inputs:
%   filename    - complete filename (including file path) of data file to
%                 be imported
%    
% Outputs:
%   NP_data     - raw EEG data of imported file
%   NP_info     - general information about imported EEG file
%   NP_marker   - marker/events of imported file
%
% See also: 
%   np_readdata(), np_readfileinfo(), np_readmarker(),
%   pop_readNEUROPRAX(), EEGLAB 
%
% Author:   Falk Schlegelmilch for the *.EEG Matlab reading functions. 
%           Sophia Wunder for the EEGLAB interface.
%
% Copyright (C) 2017 Sophia Wunder (sophia.wunder@neurocaregroup.com),
%           neuroConn GmbH, Ilmenau, Germany
%
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function [NP_data, NP_info, NP_marker] = readNEUROPRAX(filename)

if nargin < 1
	help readNEUROPRAX;
	return;
end;	

% read in all data
start=0;        % starting with the first
laenge=inf;     % ending with the last
typ='samples';  % sample in the data

NP_data = np_readdata(filename,start,laenge,typ);
NP_info = np_readfileinfo(filename);
NP_marker = np_readmarker(filename,0,inf,'samples');