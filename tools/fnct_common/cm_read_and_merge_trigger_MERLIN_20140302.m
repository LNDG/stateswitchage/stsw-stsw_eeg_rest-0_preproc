function cm_read_and_merge_trigger_MERLIN_20140302(path,ID,BLOCK)
%
% path requires following fields: 
% .hst = history folder
% .beh = raw behavioral data folder
% .eeg = raw eeg data folder
% .out = processed behavioral data folder

%% load behavioral data

if exist([path.out 'Merlin_' num2str(ID) '.mat'],'file')
    load([path.out 'Merlin_' num2str(ID) '.mat'],'indData')
else
    load([path.beh 'Merlin_' num2str(ID) '.mat'])
end

%% prep loading eeg data

% load config for eeg data file names
if exist([path.hst num2str(ID) '_config_' BLOCK '.mat'],'file')
    load([path.hst num2str(ID) '_config_' BLOCK],'config')
% alternatively generate file names directly
else
    file_data = dir([path.eeg '*' BLOCK '.eeg']);
    file_head = dir([path.eeg '*' BLOCK '.vhdr']);
    config.data_file = file_data.name;
    config.header_file = file_head.name;
    clear file_*
end

%% check if merging completed

check = 1;
if strcmp(BLOCK,'L')
    if size(indData,2) >= 43; if strcmp(indData{1,43},['trig_' BLOCK]);
        check = 0;
    end; end
elseif strcmp(BLOCK,'R1')
    if size(indData,2) >= 44; if strcmp(indData{1,44},['trig_' BLOCK]);
        check = 0;
    end; end
elseif strcmp(BLOCK,'R2')
    if size(indData,2) >= 45; if strcmp(indData{1,45},['trig_' BLOCK]);
        check = 0;
    end; end
elseif strcmp(BLOCK,'R3')
    if size(indData,2) >= 46; if strcmp(indData{1,46},['trig_' BLOCK]);
        check = 0;
    end; end
end

%% check if EEG data exists

if isempty(config.data_file)
    check = 0;
end

%% merge if not completed already

if check

% open log file
fid = fopen([path.hst 'log_' num2str(ID) '_trigger.txt'],'a');
    
% write log
fprintf(fid,['*********************  ' BLOCK '  *********************\n']);
fprintf(fid,['function: cm_read_and_merge_trigger_MERLIN_20140302.m \n\n']);
fprintf(fid,['beh file = Merlin_' num2str(ID) '.mat\n']);

%% load trigger data

% write log
fprintf(fid,['eeg file = ' config.data_file '\n']);
   
% get column index & behavioral time stamps
if strcmp(BLOCK,'L')
    ind = find(strcmp(indData(1,:),'trig_encoding'));
    X   = cell2mat(indData(2:end,ind));
    display(['behavioral data (' BLOCK '): ' num2str(length(X)) ' trials found']);
    fprintf(fid,['beh data (' BLOCK '): ' num2str(length(X)) ' trials found\n']);
elseif strcmp(BLOCK,'R1')
    ind = find(strcmp(indData(1,:),'trig_recall1'));
    X   = cell2mat(indData(2:end,ind));
    display(['behavioral data (' BLOCK '): ' num2str(length(X)) ' trials found']);
    fprintf(fid,['beh data (' BLOCK '): ' num2str(length(X)) ' trials found\n']);
elseif strcmp(BLOCK,'R2')
    ind = find(strcmp(indData(1,:),'trig_recall2'));
    X   = cell2mat(indData(2:end,ind));
    display(['behavioral data (' BLOCK '): ' num2str(length(X)) ' trials found']);
    fprintf(fid,['beh data (' BLOCK '): ' num2str(length(X)) ' trials found\n']);
elseif strcmp(BLOCK,'R3')
    ind = find(strcmp(indData(1,:),'trig_recall3'));
    X   = cell2mat(indData(2:end,ind));
    display(['behavioral data (' BLOCK '): ' num2str(length(X)) ' trials found']);
    fprintf(fid,['beh data (' BLOCK '): ' num2str(length(X)) ' trials found\n']);
end

%% trigger detection

% cfg for trigger detection
cfg.datafile    = [path.eeg config.data_file];
cfg.headerfile  = [path.eeg config.header_file];
cfg.trigchan    = 'Trig';
cfg.direction   = -1;       % check for downward-going deflections
cfg.mindiff     = 0.002;    % at least 2 ms between two successive 'trigger'
% cfg.maxdiff     = 8;      % between two 'trigger' should not be more than cfg.maxdiff seconds
cfg.visualcheck = 0;

% get trigger
T = cm_get_trigger_from_channel_20140205(cfg);

% display number of extracted trigger
display(['eeg data (' BLOCK '): ' num2str(length(T)) ' trigger found']);
fprintf(fid,['eeg data (' BLOCK '): ' num2str(length(T)) ' trigger found\n']);

%% check trigger

% same size
if ~(length(X) == length(T))
    warning('--> trigger/trial missing!')
    check_size = 0;
    fprintf(fid,['--> trigger/trial missing!\n']);
else
    check_size = 1;
end

% cross-correlation
[c,lags] = xcorr(sortrows(X),T);
lag = lags(find(c==max(c)));
r = corr(sortrows(X(1+lag:length(T)+lag)),T);

%% combine trigger

% keep behavioral time stamps
C = X;
C = sortrows(C,1);

% add NaNs
C(:,2) = NaN;

% add trigger
C(1+lag:length(T)+lag,2) = T;

% check merging
r_ = corr(C(:,1),C(:,2),'rows','pairwise');

% display correlation
display(['test correlation          = ' num2str(r)])
display(['correlation after merging = ' num2str(r_)])
fprintf(fid,['test correlation          = ' num2str(r) '\n']);
fprintf(fid,['correlation after merging = ' num2str(r_) '\n']);

% check correlation
if r < .999
    check_corr = 0;
    warning('Deficient match of time stamps and trigger!')
    fprintf(fid,['--> deficient match of time stamps and trigger!\n']);
else
    check_corr = 1;
end

%% include trigger in summary table

% generate empty trigger cell array
trig = cell(size(indData,1),1);

% match trigger in table
for l = 2:size(indData,1)
    if ~isempty(indData{l,ind})
        get = find(C(:,1)==indData{l,ind});
        trig{l,1} = C(get,2);
    end
end; clear l

% header
trig{1,1} = ['trig_' BLOCK];

% include trigger in table
if strcmp(BLOCK,'L')
    indData(:,43) = trig;
    r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,43)),'rows','pairwise');
    display(['final merging check       = ' num2str(r3)]);
    fprintf(fid,['final merging check       = ' num2str(r3) '\n\n']);
elseif strcmp(BLOCK,'R1')
    indData(:,44) = trig;
    r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,44)),'rows','pairwise');
    display(['final merging check       = ' num2str(r3)]);
    fprintf(fid,['final merging check       = ' num2str(r3) '\n\n']);
elseif strcmp(BLOCK,'R2')
    indData(:,45) = trig;
    r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,45)),'rows','pairwise');
    display(['final merging check       = ' num2str(r3)]);
    fprintf(fid,['final merging check       = ' num2str(r3) '\n\n']);
elseif strcmp(BLOCK,'R3')
    indData(:,46) = trig;
    r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,46)),'rows','pairwise');
    display(['final merging check       = ' num2str(r3)]);
    fprintf(fid,['final merging check       = ' num2str(r3) '\n\n']);
end
    
%% save data file with trigger

% save behavioral data
save([path.out 'Merlin_' num2str(ID) '.mat'],'indData')

%% close log file
fclose(fid);

else
    
    display('--> merging already previously completed')
    
end
    
    