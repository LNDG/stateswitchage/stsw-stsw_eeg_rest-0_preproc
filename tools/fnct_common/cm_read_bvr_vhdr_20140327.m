function info = cm_read_bvr_vhdr_20140327(pathname,filename)

A = textread([pathname '/' filename],'%s','delimiter','\n');

%% keep header file name
info.header_file = filename;

%% get general information
for j = 1:length(A)

    if isempty(regexp(A{j,1}, 'DataFile')) == 0
        ind = regexp(A{j,1}, '=');
        info.data_file = A{j,1}(ind+1:end);
    end
    if isempty(regexp(A{j,1}, 'MarkerFile')) == 0
        ind = regexp(A{j,1}, '=');
        info.marker_file = A{j,1}(ind+1:end);
    end
    if isempty(regexp(A{j,1}, 'DataFormat')) == 0
        ind = regexp(A{j,1}, '=');
        info.data_format = A{j,1}(ind+1:end);
    end
    if isempty(regexp(A{j,1}, 'BinaryFormat')) == 0
        ind = regexp(A{j,1}, '=');
        info.binary_format = A{j,1}(ind+1:end);
    end
    % number of channels
    if isempty(regexp(A{j,1}, 'NumberOfChannels')) == 0
        ind = regexp(A{j,1}, '=');
        info.nchan = str2num(A{j,1}(ind+1:end));
    end
    % number of data points
    if isempty(regexp(A{j,1}, 'DataPoints')) == 0
        ind = regexp(A{j,1}, '=');
        info.pnts = str2num(A{j,1}(ind+1:end));
    end
    % sampling rate
    if isempty(regexp(A{j,1}, 'SamplingInterval=')) == 0
        ind = regexp(A{j,1}, '=');
        info.srate = 1./(str2num(A{j,1}(ind+1:end))/1e6);
    end
            
end; clear j
   

%% clear workspace
clear A filename