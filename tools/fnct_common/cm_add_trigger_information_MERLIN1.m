function cm_add_trigger_information_MERLIN1(path,ID)
%
% path requires following fields:
% .hst = history folder
% .beh = raw behavioral data folder
% .eeg = raw eeg data folder
% .out = processed behavioral data folder

%% load behavioral data

load([path.beh 'Merlin_' num2str(ID) '.mat'])

BLOCK = {'L','R1','R2','R3'};

for b = 1: length(BLOCK) 

%% prep loading eeg data
%
% % load config for eeg data file names
if exist([path.hst num2str(ID) '_config_' BLOCK{b} '.mat'],'file')
load([path.hst num2str(ID) '_config_' BLOCK{b}],'config')
% alternatively generate file names directly
else
    file_data = dir([path.eeg '*' BLOCK{b} '.eeg']);
    file_head = dir([path.eeg '*' BLOCK{b} '.vhdr']);
    config.data_file = file_data.name;
    config.header_file = file_head.name;
    clear file_*
end

%% check if merging completed & if triggers missing...

check = 1;
if strcmp(BLOCK{b},'L')
    if sum(isnan(cell2mat(indData(2:end,15)))) == 0
        check = 0;
    end; 
elseif strcmp(BLOCK{b},'R1')
    if sum(isnan(cell2mat(indData(2:end,16)))) == 0
        check = 0;
    end;
elseif strcmp(BLOCK{b},'R2')
    if sum(isnan(cell2mat(indData(2:end,17)))) == 0
        check = 0;
    end; 
elseif strcmp(BLOCK{b},'R3')
    if sum(isnan(cell2mat(indData(2:end,18)))) == 0
        check = 0;
    end; 
end

% %% check if EEG data exists
%
if isempty(config.data_file)
    check = 0;
end

%% merge if not completed already

%if check
    
    % open log file
    fid = fopen([path.hst 'log_' num2str(ID) '_trigger.txt'],'a');
    
    % write log
    fprintf(fid,['*********************  ' BLOCK{b} '  *********************\n']);
    fprintf(fid,['function: cm_add_trigger_information_MERLIN1.m \n\n']);
    fprintf(fid,['beh file = Merlin_' num2str(ID) '.mat\n']);
    
    %% load trigger data
    
    % write log
    fprintf(fid,['eeg file = ' config.data_file '\n']); % here data file is the original file
    
    % get column index & behavioral time stamps
    if strcmp(BLOCK{b},'L')
        ind = find(strcmp(indData(1,:),'trig_encoding'));
        X   = cell2mat(indData(2:end,ind));
        display(['behavioral data (' BLOCK{b} '): ' num2str(length(X)) ' trials found']);
        fprintf(fid,['beh data (' BLOCK{b} '): ' num2str(length(X)) ' trials found\n']);
   elseif strcmp(BLOCK{b},'R1')
        ind = find(strcmp(indData(1,:),'trig_recall1'));
        X   = cell2mat(indData(2:end,ind));
        display(['behavioral data (' BLOCK{b} '): ' num2str(length(X)) ' trials found']);
        fprintf(fid,['beh data (' BLOCK{b} '): ' num2str(length(X)) ' trials found\n']);
    elseif strcmp(BLOCK{b},'R2')
        ind = find(strcmp(indData(1,:),'trig_recall2'));
        X   = cell2mat(indData(2:end,ind));
        display(['behavioral data (' BLOCK{b} '): ' num2str(length(X)) ' trials found']);
        fprintf(fid,['beh data (' BLOCK{b} '): ' num2str(length(X)) ' trials found\n']);
    elseif strcmp(BLOCK{b},'R3')
        ind = find(strcmp(indData(1,:),'trig_recall3'));
        X   = cell2mat(indData(2:end,ind));
        display(['behavioral data (' BLOCK{b} '): ' num2str(length(X)) ' trials found']);
        fprintf(fid,['beh data (' BLOCK{b} '): ' num2str(length(X)) ' trials found\n']);
    end
    
    %% trigger detection
    
    % cfg for trigger detection
    cfg.datafile    = [path.eeg config.data_file];
    cfg.headerfile  = [path.eeg config.header_file];
    cfg.trigchan    = 'Trig';
    cfg.direction   = -1;       % check for downward-going deflections
    cfg.mindiff     = 0.002;    % at least 2 ms between two successive 'trigger'
    % cfg.maxdiff     = 8;      % between two 'trigger' should not be more than cfg.maxdiff seconds
    cfg.visualcheck = 0;
    
    % get trigger
    T = cm_get_trigger_from_channel_20140319(cfg);
    
    % display number of extracted trigger
    display(['eeg data (' BLOCK{b} '): ' num2str(length(T)) ' trigger found']);
    fprintf(fid,['eeg data (' BLOCK{b} '): ' num2str(length(T)) ' trigger found\n']);
    
    %% check trigger
    
    % same size
    if length(X) > length(T)
        warning('--> trigger/trial missing!')
        check_size = 0;
        fprintf(fid,['--> trigger/trial missing!\n']);
    elseif length(X) < length(T)
        warning('--> too many triggers!')
        check_size = 0;
        fprintf(fid,['--> too many triggers!\n']);
    else
        check_size = 1;
    end
    
    % cross-correlation
    [c,lags] = xcorr(sortrows(X),T);
    lag = lags(find(c==max(c)));
    
    % correct triggers if too many...
    if length(X) < length(T) && lag < 0
        T(1:abs(lag)) = [];
        lag = 0;
    end
    
    % correlation coefficient
    r = corr(sortrows(X(1+lag:length(T)+lag)),T);
    
    %% combine trigger
    
    % keep behavioral time stamps
    C = X;
    C = sortrows(C,1);
    
    % add NaNs
    C(:,2) = NaN;
    
    % add trigger
    C(1+lag:length(T)+lag,2) = T;
    
    % check merging
    r_ = corr(C(:,1),C(:,2),'rows','pairwise');
    
    % display correlation
    display(['test correlation          = ' num2str(r)])
    display(['correlation after merging = ' num2str(r_)])
    fprintf(fid,['test correlation          = ' num2str(r) '\n']);
    fprintf(fid,['correlation after merging = ' num2str(r_) '\n']);
    
    % check correlation
    if r < .999
        check_corr = 0;
        warning('Deficient match of time stamps and trigger!')
        fprintf(fid,['--> deficient match of time stamps and trigger!\n']);
    else
        check_corr = 1;
    end
    
    %% include trigger in summary table
    
    % generate empty trigger cell array
    trig = cell(size(indData,1),1);
    
    % match trigger in table
    for l = 2:size(indData,1)
        if ~isempty(indData{l,ind})
            get = find(C(:,1)==indData{l,ind});
            trig{l,1} = C(get,2);
        end
    end; clear l
    
    % header
    trig{1,1} = ['trig_' BLOCK{b}];
    
    % include trigger in table
    if strcmp(BLOCK{b},'L')
        indData(:,43) = trig;
        %indData(:,66) = trig
        r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,43)),'rows','pairwise');
        %r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,66)),'rows','pairwise');
        display(['final merging check       = ' num2str(r3)]);
        fprintf(fid,['final merging check       = ' num2str(r3) '\n\n']);
    elseif strcmp(BLOCK{b},'R1')
        indData(:,44) = trig;
        %indData(:,67) = trig
        r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,44)),'rows','pairwise');
        %r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,21)),'rows','pairwise');
        display(['final merging check       = ' num2str(r3)]);
        fprintf(fid,['final merging check       = ' num2str(r3) '\n\n']);
    elseif strcmp(BLOCK{b},'R2')
        indData(:,45) = trig;
        %indData(:,68) = trig
        r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,45)),'rows','pairwise');
        %r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,22)),'rows','pairwise');
        display(['final merging check       = ' num2str(r3)]);
        fprintf(fid,['final merging check       = ' num2str(r3) '\n\n']);
    elseif strcmp(BLOCK{b},'R3')
        indData(:,46) = trig;
        %indData(:,69) = trig
        r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,46)),'rows','pairwise');
        %r3 = corr(cell2mat(indData(2:end,ind)),cell2mat(indData(2:end,23)),'rows','pairwise');
        display(['final merging check       = ' num2str(r3)]);
        fprintf(fid,['final merging check       = ' num2str(r3) '\n\n']);
    end
end;   
    %% save data file with trigger
    
    % save behavioral data
    save([path.out 'Merlin_' num2str(ID) '.mat'],'indData')
    
    %% close log file
    fclose(fid);
    
    %else
    
    %display('--> merging already previously completed')
    
%end

