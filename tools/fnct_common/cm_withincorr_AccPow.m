function outdat = cm_withincorr_AccPow(cfg, indat, trlvec)

%--------------------------------------------------------------------------
%
% CM_WITHINCORR_ACCPOW performs within subject point-biserial correlations
% between power and accuracy measures
%
% indat  = structure from ft_freqanalysis containing tfr-data
% trlvec = vector containing binary data reflecting the outcome variable in
% each trial
% cfg = structure with additional settings (can be empty, i.e., [])
%
% MWB, 10/2014
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% check defaults ...
%--------------------------------------------------------------------------
if ~isfield(cfg,'parameter'),     cfg.parameter = 'powspctrm'; end
if ~isfield(cfg,'bootstrapflag'), cfg.bootstrapflag = 1;       end % perform bootstrap
if ~isfield(cfg,'nrboot'),        cfg.nrboot = 200;            end % default nr of bootstraps
if ~isfield(cfg,'parallel'),      cfg.parallel = 0;            end % use parallel computing options ...
if ~isfield(cfg,'nrworkers'),     cfg.nrworkers = 2;           end % use two cores if parallel computing is used
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% collect basic information ...
%--------------------------------------------------------------------------
nr.trl  = size(indat.(cfg.parameter),1);
nr.chan = size(indat.(cfg.parameter),2);
nr.freq = size(indat.(cfg.parameter),3);
nr.time = size(indat.(cfg.parameter),4);

% check consitency
if length(trlvec) ~= nr.trl, error('not the same number of trials in data and trlvec \n'); end
if (sum(trlvec == 0) + sum(trlvec == 1)) ~= nr.trl, error('trlvec must only contain 0 and 1 values \n'); end
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% compute point-biserial correlation ...
%--------------------------------------------------------------------------
trlvec = logical(trlvec);

% length of group 1
n1 = sum(trlvec);
% length of group 0
n0 = sum(~trlvec);

% compute means for each group
m1 = squeeze(mean(indat.(cfg.parameter)(trlvec,:,:,:),1));
m0 = squeeze(mean(indat.(cfg.parameter)(~trlvec,:,:,:),1));

% sd
sx = squeeze(std(indat.(cfg.parameter),[],1));

% correlation coefficient
r_orig = ((m1 - m0) ./ sx) .* sqrt((n1.*n0)./nr.trl.^2);
clear m1 m0 sx n1 n0;
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% compute boostraps
%--------------------------------------------------------------------------
if cfg.bootstrapflag
    
    % pre-allocate memory
    r_boot = zeros(cfg.nrboot,nr.chan,nr.freq,nr.time);
    
    if cfg.parallel
        matlabpool('local',cfg.nrworkers);        
        parfor i1 = 1:cfg.nrboot
            
            fprintf('running bootstrap %d from %d \n',i1,cfg.nrboot);
            
            rs = [];
            rs = randsample(nr.trl,nr.trl,true);
            
            bootvec = [];
            bootvec = trlvec(rs);
            
            % compute correlation
            n1 = []; n0 = []; m1 = []; m0 =  []; sx = [];

            % length of group 1
            n1 = sum(bootvec);
            % length of group 0
            n0 = sum(~bootvec);
            
            % compute means for each group
            m1 = squeeze(mean(indat.(cfg.parameter)(rs(bootvec),:,:,:),1));
            m0 = squeeze(mean(indat.(cfg.parameter)(rs(~bootvec),:,:,:),1));
            
            % sd
            sx = squeeze(std(indat.(cfg.parameter)(rs,:,:,:),[],1));
            
            % correlation coefficient
            r_boot(i1,:,:,:) = ((m1 - m0) ./ sx) .* sqrt((n1.*n0)./(nr.trl.^2));
        end
        matlabpool close;
    else
        
        for i1 = 1:cfg.nrboot
            
            fprintf('running bootstrap %d from %d \n',i1,cfg.nrboot);
            
            rs = [];
            rs = randsample(nr.trl,nr.trl,true);
            
            bootvec = [];
            bootvec = trlvec(rs);
            
            % compute correlation
            % length of group 1
            n1 = sum(bootvec);
            % length of group 0
            n0 = sum(~bootvec);
            
            % compute means for each group
            m1 = squeeze(mean(indat.(cfg.parameter)(rs(bootvec),:,:,:),1));
            m0 = squeeze(mean(indat.(cfg.parameter)(rs(~bootvec),:,:,:),1));
            
            % sd
            sx = squeeze(std(indat.(cfg.parameter)(rs,:,:,:),[],1));
            
            % correlation coefficient
            r_boot(i1,:,:,:) = ((m1 - m0) ./ sx) .* sqrt((n1.*n0)./(nr.trl.^2));
            clear m1 m0 sx n1 n0;
            
        end
    end
    clear rs bootvec i1;
    
    % compute associated p-value and se
    r_p = zeros(size(r_orig));
    for i1 = 1:nr.chan
        for i2 = 1:nr.freq
            for i3 = 1:nr.time
                
                r_tmp = [];
                r_tmp = squeeze(r_orig(i1,i2,i3));
                
                if ~isnan(r_tmp) & ~isempty(r_tmp)
                    
                    bootdis_tmp = [];
                    bootdis_tmp = squeeze(r_boot(:,i1,i2,i3));
                    
                    if r_tmp < 0

                        r_p(i1,i2,i3) = sum(bootdis_tmp >= 0) ./ cfg.nrboot;
                        
                    elseif r_tmp >= 0
                        
                        r_p(i1,i2,i3) = sum(bootdis_tmp <= 0) ./ cfg.nrboot;
                        
                    elseif r_tmp == 0
                        
                        r_p(i1,i2,i3) = 1;
                        
                    else
                        error('unknown result ...\n');
                        
                    end
                    
                else
                    r_p(i1,i2,i3) = NaN;
                end
            end
        end
    end
    clear i1 i2 i3 bootdis_tmp r_tmp
end
%--------------------------------------------------------------------------


%--------------------------------------------------------------------------
% finalize output
%--------------------------------------------------------------------------
outdat            = rmfield(indat,cfg.parameter); % pass information from original structure
outdat.dimord     = 'chan_freq_time';
outdat.pb_corr    = r_orig; % raw correlation coefficients
outdat.pb_corr_se = squeeze(std(r_boot,[],1)./sqrt(cfg.nrboot)); % standard error for correlation as derived from bootstrap
outdat.zval       = tanh(r_orig);    % Fisher-z-values
outdat.bootPval   = r_p;    % p-value from bootstrap distribution
%--------------------------------------------------------------------------







