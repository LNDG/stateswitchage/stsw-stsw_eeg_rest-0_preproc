function outdat = mcs_realSigchange_4D(indat, idxBaseline, zscoreflag)

% variation of mwb_realSigchange
% computes post-stim-power change from baseline
%
% idxBaseline = indices of baseline-time points
% indat = matrix of size trials X time
% if zscoreflag == 1 --> compute z-scores

if nargin < 3
    zscoreflag = 0;
end

[nSubj,Chan,Freq,nTime] = size(indat);


% for each trial:
mB = squeeze(nanmean(indat(:,:,:,idxBaseline),4)); % mean across baseline period (subj, chan, time)

% dummy Matrices
MB = repmat(mB,[1,1,1,nTime]); clear mB;

if ~zscoreflag
    
    % compute relchange
    outdat = (indat - MB) ./ MB;
    
else
    
    % for each trial:
    sB = squeeze(nanstd(indat(:,:,:,idxBaseline),[],4)); % std across baseline period
    
    % dummy Matrices
    SB = repmat(sB,[1,1,1,nTime]); clear sB;
    
    % compute zscores
    outdat = (indat - MB) ./ SB;
    
end





  



