function [elecnew] = cm_elec2dataelec_20170919(elecorig, data)

% [elecnew] = elec2data(elecorig, data)
% 
% elec2data compares the elctrode location specified in data.label with
% a electrode position structure as given in elecorig. The output is a new
% elecstructure that only contains the electrode labels and positions as
% given in data.label. elecnew can for example be added to the
% data-structure (as data.elec) or can be used as cfg.layout in 
% plot-functions

% MWB
% JQK 170919 added IOR (infra-orbital right) option

if isfield(data,'topolabel')
    data.label = data.topolabel;
end

if ~isfield(data,'label')
    error('data does not contain a label-field \n');
end

for ind = 1:length(data.label)
    
    elecnew.label(ind,1) = data.label(ind);
    
    % some corrections due to differences in naming conventions
    if strcmp(data.label{ind},'A1')
        data.label(ind) = {'M1'};
    elseif strcmp(data.label{ind},'A2')
        data.label(ind) = {'M2'};
    end
    
    % rename EOG electrodes in order to plot them
    if strcmp(data.label{ind},'LHEOG')
        data.label(ind) = {'AF9'};
    elseif strcmp(data.label{ind},'RHEOG')
        data.label(ind) = {'AF10'};
    elseif strcmp(data.label{ind},'IOL')
        data.label(ind) = {'AFp9'};
    elseif strcmp(data.label{ind},'IOR')
        data.label(ind) = {'AFp10'};
    end
    
    elecidc = find(strcmp(elecorig.label(:,1),data.label(ind)));
    
    if isempty(elecidc)
        error('labels in elecorig and labels in data do not match \n');
    else
        elecnew.pnt(ind,:) = elecorig.pnt(elecidc,:);
    end
    clear elecidc
    
end


