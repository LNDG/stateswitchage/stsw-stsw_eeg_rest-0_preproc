function [seg srate] = cm_read_marked_segments_bva_20140327(path,file)
%
% [seg srate] = read_MERLIN_140113(path,file)
%
% reads indices of visually selected (in BVA) periods of EEG, contaminated 
% by artefacts
%
% path = path to output .txt file from BVA
% file = output file name of BVA .txt file

% 26.02.2014 THG

% 27.03.2014 THG
% changes: if there are no segments specified, an empty variable seg will 
% be given as output

%%  open text file & get data

    % open file
    fid = fopen([path '/' file]);
    C = textscan(fid,'%s\t');
    fclose(fid); clear fid

    % get data
    data = C{1,1}; clear C

%%  get sampling rate - to check

    % get index of sampling rate
    get1 = regexp(data,'Sampling');
    get2 = regexp(data,'rate:');
    for i = 1:length(get1)-1
        get3(i,1) = (~isempty(get1{i,1}) & ~isempty(get2{i+1,1}));
    end
    ind_sr = find(get3 == 1) + 2; 

    % extract sampling rate
    srate = cell2mat(textscan(data{ind_sr},'%n'));

    % clear variables
    clear i get* ind_sr
    
%%  get time stamps & segment lengths

    % get indices of segments
    get1 = regexp(data,'Userdefined');
    for i = 1:length(get1)
        get2(i,1) = (~isempty(get1{i,1}));
    end; clear i
    ind_seg = find(get2 == 1); 
    
    % extract segments if indices exist
    if ~isempty(ind_seg)
        for j = 1:length(ind_seg)
            seg(j,1) = cell2mat(textscan(data{ind_seg(j,1)+1},'%n'));
            seg(j,2) = cell2mat(textscan(data{ind_seg(j,1)+2},'%n'));
        end
    % otherwise generate empty variable seg
    elseif isempty(ind_seg)
        seg = [];
    end
    
    % clear variables
    clear i j get* ind_seg
