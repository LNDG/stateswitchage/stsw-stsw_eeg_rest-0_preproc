function elec_new = cm_select_channels_from_elec(elec,sel)

% elec = elec-strcuture to select from
% sel = cell-array input for ft_channelselection

tmp.chan = ft_channelselection(sel,elec.label);

% find the indices for the selected channels in elec
for i1 = 1:length(tmp.chan)
    
    tmp.idx(i1,1) = find(strcmp(tmp.chan{i1},elec.label));
    
end

% finalize output
elec_new.label = elec.label(tmp.idx);
if isfield(elec,'chanpos'),  elec_new.chanpos  = elec.chanpos(tmp.idx,:);  end
if isfield(elec,'chantype'), elec_new.chantype = elec.chantype(tmp.idx,:); end
if isfield(elec,'chanunit'), elec_new.chanunit = elec.chanunit(tmp.idx,:); end
if isfield(elec,'elecpos'),  elec_new.elecpos  = elec.elecpos(tmp.idx,:);  end
if isfield(elec,'type'),     elec_new.type     = elec.type;                end
if isfield(elec,'unit'),     elec_new.unit     = elec.unit;                end
if isfield(elec,'cfg'),      elec_new.cfg      = elec.cfg;                 end
    
    