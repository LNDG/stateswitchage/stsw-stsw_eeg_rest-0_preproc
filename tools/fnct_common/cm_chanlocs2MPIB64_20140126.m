function data = cm_chanlocs2MPIB64_20140126(data,chanlocs)
%
% map & add EEGLAB chanlocs structure to current fieldtrip data structure 
%
% data = fieldtrip data structure
% chanlocs = EEGLAB chanloc structure containing all required electrodes

% 26.01.2014 THG

for j = 1:length(data.label)
    
    % get index of jth channel
    index = [];
    for k = 1:length(chanlocs)
        if strcmp(data.label{j},chanlocs(k).labels)
            index = k;
        end    
    end; clear k
    
    % get EEGLAB channel information
    if ~isempty(index)
        data.chanlocs(1,j) = chanlocs(1,index);
    else
        data.chanlocs(1,j).labels = data.label{j};
    end
    
    % clear index
    clear index
    
end; clear j
