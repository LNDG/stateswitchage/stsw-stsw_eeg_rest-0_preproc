function info = cm_read_bvr_vhdr_20140226(pathname,filename)

A = textread([pathname '/' filename],'%s','delimiter','\n');

%% keep header file name
info.header_file = filename;

%% get general information
for j = 1:length(A)

    if isempty(regexp(A{j,1}, 'DataFile')) == 0
        ind = regexp(A{j,1}, '=');
        info.data_file = A{j,1}(ind+1:end);
    end
    if isempty(regexp(A{j,1}, 'MarkerFile')) == 0
        ind = regexp(A{j,1}, '=');
        info.marker_file = A{j,1}(ind+1:end);
    end
    if isempty(regexp(A{j,1}, 'DataFormat')) == 0
        ind = regexp(A{j,1}, '=');
        info.data_format = A{j,1}(ind+1:end);
    end
    if isempty(regexp(A{j,1}, 'BinaryFormat')) == 0
        ind = regexp(A{j,1}, '=');
        info.binary_format = A{j,1}(ind+1:end);
    end
    % number of channels
    if isempty(regexp(A{j,1}, 'Number of channels')) == 0
        ind = regexp(A{j,1}, ':');
        info.nchan = str2num(A{j,1}(ind+1:end));
        % number of data points
        memmap = memmapfile([pathname '/' filename(1:end-5) '.eeg'], ...
                     'Format', 	'int16', ...
                     'Writable', false);
        info.pnts = length(memmap.Data)/info.nchan;
        clear memmap
    end
    if isempty(regexp(A{j,1}, 'Sampling Rate')) == 0
        ind = regexp(A{j,1}, ':');
        info.srate = str2num(A{j,1}(ind+1:end));
    end
    if j > 1
        if strcmp(A{j-1,1},'Channels') && strcmp(A{j,1},'--------')
            ind_chan_info = j + 2;
        end
        if ~isempty(regexp(A{j,1}, 'Impedance '))
            ind_imp_info = j + 1;
            ind = regexp(A{j,1}, 'at');
            info.imp_check_time = A{j,1}(ind+3:ind+10);
        elseif ~isempty(regexp(A{j,1}, 'No impedance values available'))
            ind = regexp(A{j,1}, 'at');
            info.imp_check_time = A{j,1}(ind+3:ind+10);
            info.impedance = 'No impedance values available!';
        end
        if ~isempty(regexp(A{j,1}, 'Gnd:'))
            ind_imp_info2 = j;
        end
    end
            
end; clear j
   
%% get impedance infos from header
if ~isfield(info,'impedance')
    cnt = 1;
    for j = ind_imp_info : ind_imp_info2
        tmp = A{j,1};
        ind = regexp(tmp, ':');
        imp_info{cnt,1} = A{j,1}(    1 :ind(1)-1);
        if strcmp(tmp(1:3),'ECG') == 0
            imp_info{cnt,2} = str2num(A{j,1}(ind(1)+1:end));
        else
            imp_info{cnt,2} = NaN;
        end
        if isempty(imp_info{cnt,2})
            imp_info{cnt,2} = NaN;
        end
        clear tmp ind
        cnt = cnt + 1;
    end; clear j cnt

    tmp(2:length(imp_info)+1,:) = imp_info;
    tmp(1,1:2) = {'ChanLabel','Impedance [kOhm]'};

    info.impedance = tmp;
    clear tmp imp_info ind_imp_info ind_imp_info2
end 


%% get channel infos from header
cnt = 1;
for j = ind_chan_info : ind_chan_info + info.nchan - 1
    tmp = A{j,1};
    ind = regexp(tmp, ' ');
    for k = 1:length(ind)-1
        if ind(k) == ind(k+1)-1;
            ind(k) = 0;
        end
    end; clear k
    ex = find(ind == 0);
    ind(ex) = [];
    % get channel info
    chan_info{cnt,1} = str2num(A{j,1}(    1 :ind(1)));
    chan_info{cnt,2} = A{j,1}(ind(1):ind(2));
    chan_info{cnt,3} = str2num(A{j,1}(ind(2):ind(3)));
    chan_info{cnt,4} = str2num(A{j,1}(ind(3):ind(4)));
    chan_info{cnt,5} = A{j,1}(ind(4):ind(5));
    chan_info{cnt,6} = A{j,1}(ind(5):ind(6));
    chan_info{cnt,7} = str2num(A{j,1}(ind(6):ind(7)));
    chan_info{cnt,8} = A{j,1}(ind(7): end);
    for l = [2 5 6 8]
        tmp = chan_info{cnt,l};
        ex = regexp(tmp, ' ');
        tmp(ex) = [];
        chan_info{cnt,l} = tmp;
        clear tmp ex
    end; clear l
    cnt = cnt + 1;
    clear ind
end; clear j cnt
    
tmp(2:length(chan_info)+1,:) = chan_info;
tmp(1,1:7) = {'#','ChanLabel','PhysChan','Resolution [\muV]','','Low Cutoff [s]','High Cutoff [Hz]'};
tmp(:,[5 8]) = [];

info.chan_info = tmp;
clear tmp ans chan_info ind_chan_info

%% clear workspace
clear A filename