function cm_preprocessing_1_MERLIN_20140327(path,funs,ID,BLOCK)

% required functions:
% - cm_read_marked_segments_bva_20140226
% - cm_read_bvr_vhdr_20140226
% - cm_remove_edges_20140122
% - cm_automatic_IC_detection_20140226
% - cm_arbitrary_segmentation_fieldtrip_20140126
% - cm_elec2dataelec_20140226
% - cm_chanlocs2MPIB64_20140126

%% generate paths from general path

% path: history (e.g., EEG marked segments)
pn.hst = [path '/' num2str(ID) '/history/'];

% path: raw EEG data 
pn.dat = [path '/' num2str(ID) '/raw_eeg/'];

% path: processed eeg data
pn.eeg = [path '/' num2str(ID) '/eeg/'];

%%  read marked segments

% file with marked segments
tmp = dir([pn.hst '*_' BLOCK '_badSegs*']);
fn  = tmp.name; clear tmp

% read marked segments
seg = cm_read_marked_segments_bva_20140327(pn.hst,fn);

% generate "trial" matrix - UPDATE: get number of all data points and cut
% out marked segments!!!
if ~isempty(seg)
    seg(:,1) = seg(:,1);
    seg(:,2) = seg(:,1) + seg(:,2)-1;
    seg(:,3) = 0;
end

% clear variables
clear fn tmp

%%  get file names of EEG data

% file names
tmp.hdr = dir([pn.dat '*' BLOCK '.vhdr']);
tmp.mrk = dir([pn.dat '*' BLOCK '.vmrk']);
tmp.eeg = dir([pn.dat '*' BLOCK '.eeg']);

% include in config structure
cfg.headerfile  = [pn.dat tmp.hdr.name];
cfg.datafile    = [pn.dat tmp.eeg.name];

% get EEG data info (for number of overall data points)
info = cm_read_bvr_vhdr_20140226(pn.dat,tmp.hdr.name);

% keep info in config
config = info;

% clear variables
clear tmp

%%  define data segments

% collect data segments
if ~isempty(seg)
    for j = 1:size(seg,1)
        if j == 1
            cfg.trl(j,1) = 1;
            cfg.trl(j,2) = seg(j,1);
            cfg.trl(j,3) = 0;
        else
            cfg.trl(j,1) = seg(j-1,2);
            cfg.trl(j,2) = seg(j,1);
            cfg.trl(j,3) = 0;
        end
    end
    cfg.trl(j+1,1) = seg(j,2);
    cfg.trl(j+1,2) = info.pnts;
    cfg.trl(j+1,3) = 0;
elseif isempty(seg)
    cfg.trl(1,1) = 1;
    cfg.trl(1,2) = info.pnts;
    cfg.trl(1,3) = 0;
end

% exclude data segments smaller than 2000 ms
cnt = 1;
ex  = [];
for j = 1:size(cfg.trl,1)
    if cfg.trl(j,2) - cfg.trl(j,1) + 1 < 2000
        ex(cnt) = j;
        cnt = cnt + 1;
    end
end
cfg.trl(ex,:) = [];
        
% clear variables
clear cnt ex info j seg

%%  define reading & preprocessing parameters

cfg.channel     = {'all','-66','-67','-68','-69','-70','-71','-72','-Trig'};
cfg.continuous  = 'yes';

cfg.demean      = 'yes';

cfg.reref       = 'yes';
cfg.refchannel  = {'A1','A2'};
cfg.implicitref = 'A2';

cfg.hpfilter    = 'yes';
cfg.hpfreq      = 1;
cfg.hpfiltord   = 4;
cfg.hpfilttype  = 'but';

cfg.lpfilter    = 'yes';
cfg.lpfreq      = 125;
cfg.lpfiltord   = 4;
cfg.lpfilttype  = 'but';

%%  get data

data = ft_preprocessing(cfg);

% clear variables
clear cfg

%%  remove data

% define number of data points to remove
cfg.rm = [250 250];

% remove data points at beginning and end of segments
data = cm_remove_edges_20140122(data,cfg);
data = ft_checkdata(data,'feedback','yes');

% clear variables
clear cfg

%%  resampling before ICA [250 Hz]

% define settings for resampling
cfg.resamplefs = 250;
cfg.detrend    = 'no';
cfg.feedback   = 'no';
cfg.trials     = 'all';

% resample
data = ft_resampledata(cfg,data);

% clear variables
clear cfg

%%  ICA

% date
dt = date;

% ica config
cfg.method           = 'runica';
cfg.channel          = {'all','-ECG','-A2'};
%cfg.channel          = {'all','-ECG','-A2','-Oz'};
cfg.trials           = 'all';
cfg.numcomponent     = 'all';
cfg.demean           = 'no';
cfg.runica.extended  = 1;
cfg.runica.logfile   = [pn.hst 'log_' num2str(ID) '_' BLOCK  '_ICA1_' dt '.txt'];

% run ICA
icadat = ft_componentanalysis(cfg,data);

%%  automatic ICA labeling

[iclabels] = cm_automatic_IC_detection_20140226(data,icadat);

%%  save data for ICA labeling

% - prepare: segmentation

% define settings
cfg.length = 2;
cfg.n      = 200;
cfg.type   = 'beg';

% arbitrary segmentation - segments a 2 sec
% NOTE original segments will be overwritten
data = cm_arbitrary_segmentation_fieldtrip_20140126(data,cfg);
data = ft_checkdata(data,'feedback','yes');

% - include ICA solution
data.topo 	   = icadat.topo;
data.unmixing  = icadat.unmixing;
data.topolabel = icadat.topolabel;
data.cfg       = icadat.cfg;

% - include ICA solution in config
config.ica1.date      = dt;
config.ica1.topo 	  = icadat.topo;
config.ica1.unmixing  = icadat.unmixing;
config.ica1.topolabel = icadat.topolabel;
config.ica1.cfg       = icadat.cfg;
config.ica1.iclabels.auto = iclabels;

% fieldtrip format electrode information
load([funs '\fnct_THG\electrodelayouts\realistic_1005.mat'])
data.elec = cm_elec2dataelec_20140226(realistic_1005,data);

% EEGLAB format electrode information
load([funs '\fnct_THG\electrodelayouts\chanlocs_eeglab_MPIB_64_electrodes.mat'])
data = cm_chanlocs2MPIB64_20140126(data,chanlocs);

% - include channel information in config
config.elec     = data.elec;
config.chanlocs = data.chanlocs;

% keep ICA labels
data.iclabels = iclabels;

% save data
save([pn.eeg num2str(ID) '_' BLOCK '_Rlm_Fhl_Ica'],'data')

% save config
config.preproc_version = '20140327';
save([pn.hst num2str(ID) '_config_' BLOCK],'config')

% clear variables
clear chanlocs cfg config data dt icadat iclabels realistic_1005

