function data = cm_load_and_resample_eeg_data_20150210(cfg_preprocessing,cfg_resample,nchan2read)
%
% loading data channel-wise & resamples immediately
% very slow, but useful if raw data to read is too large for RAM
%
% nchan2read = number of channels to read concurrently
%
% NOTE: currently assuming that last channel in dummy data is implicit
%       reference!

% THG 10.02.2015

%%  get dummy data

    % temporary config
    tmpcfg = cfg_preprocessing;
    
    tmpcfg.hpfilter = 'no';
    tmpcfg.lpfilter = 'no';
    tmpcfg.trl      = [1 1 0];

    % get dummy data
    dummy = ft_preprocessing(tmpcfg);
    
    % clear temporary config
    clear tmpcfg
    
%%  generate blocks to load
    
    nchan   = length(dummy.label) - 1;
    indices = 1:nchan;
    
    check = 0;
    cnt   = 1;
    while check == 0
        if nchan2read < length(indices)
            block{cnt} = indices(1:nchan2read); 
            indices(1:nchan2read) = [];
            cnt = cnt + 1;
        elseif nchan2read >= length(indices)
            block{cnt} = indices(1:end);
            check = 1;
        end
    end

%%  load single channel data - loop channels

    % initialize append command
    append_command = [];

    for e = 1:length(block)

        % temporary config
        tmpcfg = cfg_preprocessing;

        % define channel to read
        tmpcfg.channel = {dummy.label{block{e}},'A1'};

        % read data
        eval(['data' num2str(e) '= ft_preprocessing(tmpcfg);'])

        % resample data
        eval(['data' num2str(e) ' = ft_resampledata(cfg_resample,data' num2str(e) ');'])

        % remove implicit reference
        if e < length(block)
            cfg.channel = {'all','-A1','-A2'};
            eval(['data' num2str(e) '= ft_preprocessing(cfg,data' num2str(e) ');'])
        end
        
        % change data precision to single
        for t = 1:length(data1.trial)
            eval(['data' num2str(e) '.trial{t} = single(data' num2str(e) '.trial{t});'])
        end
        
        % collect append command
        append_command = [append_command ',data' num2str(e)];

        % clear temporary config
        clear tmpcfg

    end

%%  append channels

    cfg = [];
    eval(['data = ft_appenddata(cfg' append_command ');'])

