function data = cm_filt_but_hp(data,fsample,hpf,order)
%
% function data = cm_filt_but_hp(data,fsample,lpf,order)
% input:    data
%           fsample - sampling rate (Hz)
%           hpf     - highpass frequency cutoff
%           order   - filter order
%
% NOTE: treatment of filter order is equivalent to the fieldtrip toolbox as
% of 22.01.2014

% 22.01.2014 THG

[B,A]  = butter(order,hpf/(fsample/2),'high'); 
data   = filtfilt(B,A,data); clear A B
