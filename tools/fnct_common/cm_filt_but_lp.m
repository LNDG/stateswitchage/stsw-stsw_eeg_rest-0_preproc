function data = cm_filt_but_lp(data,fsample,lpf,order)
%
% function data = filt_but_lp(data,fsample,lpf,order)
% input:    data
%           fsample - sampling rate (Hz)
%           lpf     - lowpass frequency cutoff
%           order   - filter order
%
% NOTE: treatment of filter order is equivalent to the fieldtrip toolbox as
% of 22.01.2014

% 22.01.2014 THG

[B,A]  = butter(order,lpf/(fsample/2),'low'); 
data   = filtfilt(B,A,data); clear A B
