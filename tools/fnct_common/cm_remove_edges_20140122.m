function data = cm_remove_edges_20140122(data,cfg)
%
% data = cm_remove_edges(data,cfg)
%
% removes defined number of data points at beginning and end of trials
% 
% data      = fieldtrip data structure
% cfg.rm    = [n m] defines n data point at the beginning of the trial and 
%             m data points at the end of the trial that will be removed
% cfg.time  = 'yes' changes the time vectors to start with zero
%             'no' keeps the time vectors in original alignment to the data
%             [default: 'no']
%
% NOTE: This function does not yet react when trials are not long
%       enough. Please make sure yourself that this is the case.

% 22.01.2014 THG

% default settings
if ~isfield(cfg,'time')
    cfg.time = 'no';
end

% remove data & update fields
for j = 1:length(data.trial)
    
    if size(data.trial{j},2) > sum(cfg.rm)
        
        % remove data
        data.trial{j}(:,1:cfg.rm(1)) = [];
        data.trial{j}(:,end-cfg.rm(2)+1:end) = [];
        
        % re-define time vector
        if strcmp(cfg.time,'yes')
            data.time{j} = 0:1/data.fsample:(size(data.trial{j},2)-1)/data.fsample;
        end
        
        % re-define sampleinfo
        data.sampleinfo(j,1) =  data.sampleinfo(j,1) + cfg.rm(1);
        data.sampleinfo(j,2) =  data.sampleinfo(j,2) - cfg.rm(2);

        % re-define cfg.trl
        data.cfg.trl(j,1) =  data.cfg.trl(j,1) + cfg.rm(1);
        data.cfg.trl(j,2) =  data.cfg.trl(j,2) - cfg.rm(2);
            
    end 
    
end