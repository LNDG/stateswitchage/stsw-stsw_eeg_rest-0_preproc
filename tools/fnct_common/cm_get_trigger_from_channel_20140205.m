function [trigloc, trigchan] = MWB_get_trigger_from_channel(cfg)

%--------------------------------------------------------------------------
% required arguments:
%--------------------------------------------------------------------------
% cfg.headerfile
% cfg.datafile
% cfg.direction (check for the positive or negative deflections)
%
%--------------------------------------------------------------------------
% optional arguments
%--------------------------------------------------------------------------
% cfg.trigchan
% cfg.detrend
% cfg.demean
% cfg.hpfreq
% cfg.lpfreq
% cfg.precentthresh
% cfg.minstart (in seconds)
% cfg.maxend (in seconds)
% cfg.mindiff (in seconds)
% cfg.maxdiff (in seconds)
% cfg.visualcheck 
%
%--------------------------------------------------------------------------
% MWB, 05/02/2014
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% default settings and sanity checks
%--------------------------------------------------------------------------
if ~isfield(cfg,'trigchan'), cfg.trigchan = 'Trig'; end
if ~isfield(cfg,'precentthresh'), cfg.precentthresh = 0.1; end
if ~isfield(cfg,'minstart'), minstart = 0; else minstart = 1; end
if ~isfield(cfg,'maxend'), maxend = 0; else maxend = 1; end
if ~isfield(cfg,'mindiff'), mindiff = 0; else mindiff = 1; end
if ~isfield(cfg,'maxdiff'), maxdiff = 0; else maxdiff = 1; end
if ~isfield(cfg,'visualcheck'), visualcheck = 0; else visualcheck = cfg.visualcheck; end

if ~isfield(cfg,'direction'), error('which deflection to check? rise: cfg.direction = 1 or fall: cfg.direction = -1'); end
if ~isfield(cfg,'datafile') || ~isfield(cfg,'headerfile'), 
    error('all cfg-settings required: datafile and headerfile'); 
end
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% settings for ft_processing
%--------------------------------------------------------------------------
preproccfg = [];
% read only one 'trial' containing all data
preproccfg.trialdef.triallength = inf;
preproccfg.trialdef.ntrials     = 1;
preproccfg.continous            = 'yes';
% specifiy the files to read
preproccfg.datafile             = cfg.datafile;
preproccfg.headerfile           = cfg.headerfile;
% specify the channels to read
preproccfg.channel              = cfg.trigchan;
% optional settings and defaults
if ~isfield(cfg,'detrend'), preproccfg.detrend = 'yes'; else reproccfg.detrend = cfg.detrend; end
if ~isfield(cfg,'demean'), preproccfg.demean = 'no'; else reproccfg.detrend = cfg.demean; end
if ~isfield(cfg,'lpfreq'), 
    preproccfg.lpfilter = 'no'; 
else
    preproccfg.lpfilter = 'yes'; 
    preproccfg.lpfreq   = cfg.lpfreq; 
end
if ~isfield(cfg,'hpfreq'), 
    preproccfg.hpfilter = 'no'; 
else
    preproccfg.hpfilter = 'yes'; 
    preproccfg.hpfreq   = cfg.hpfreq; 
end
%--------------------------------------------------------------------------


%--------------------------------------------------------------------------
% read the data
%--------------------------------------------------------------------------
preproccfg = ft_definetrial(preproccfg);
data = ft_preprocessing(preproccfg);
sr   = data.fsample;
trigchan = data.trial{1}; clear data;
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% determine possible onsets via first derivative
%--------------------------------------------------------------------------
% first derivative
trig_diff1 = cat(2,0,diff(trigchan));
% find the cut-offs
cutoff = prctile(trig_diff1,[cfg.precentthresh,100-cfg.precentthresh]);

if cfg.direction < 0
    trigloc = find(trig_diff1 < cutoff(1));
elseif cfg.direction > 0
    trigloc = find(trig_diff1 > cutoff(2));
else
    error('something wrong with trig detection ...');
end
%--------------------------------------------------------------------------
    
%--------------------------------------------------------------------------
% use some prior knowledge to exclude impossible trigger onsets ...
%--------------------------------------------------------------------------
% maxend
if maxend, trigloc = trigloc(find(trigtime <= cfg.maxend)); end
% minstart
if minstart, trigloc = trigloc(find(trigtime >= cfg.minstart)); end
% mindiff
if mindiff
    trigtime = trigloc.*(1./sr);
    trigtime_diff = cat(2,0,diff(trigtime));
    trigloc = trigloc(trigtime_diff >= cfg.mindiff);
end
% maxdiff
if maxdiff
    trigtime = trigloc.*(1./sr);
    trigtime_diff = cat(2,diff(trigtime),0);
    trigloc = trigloc(trigtime_diff <= cfg.maxdiff);
end

%--------------------------------------------------------------------------
% finalize output
%--------------------------------------------------------------------------
tmp.s = size(trigchan); % should be row-vector
if tmp.s(1) > tmp.s(2), trigchan = trigchan'; clear tmp; end
tmp.s = size(trigloc); % should be coloumn-vector
if tmp.s(2) > tmp.s(1), trigloc = trigloc'; clear tmp;  end
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% visual result check?
%--------------------------------------------------------------------------
if visualcheck
    figure;
    plot(trigchan,'k'); hold on;
    plot(trigloc,0,'or','markersize',10,'markerfacecolor','r','markeredgecolor','w');
end
%--------------------------------------------------------------------------
    
    