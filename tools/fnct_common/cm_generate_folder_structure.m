function cm_generate_folder_structure(pn,fn)
%
% cm_generate_folder_structure(pn,ID)
%
% generates a folder structure for EEG data analysis, including folders for
% eegavioral and anatomical (MRI, Zebris) data
%
% pn = root path name where new folder structure will be generated
% fn = folder name of the folder containing the new folder structure
%      fn can be a string or numeric

% 22.01.2014 THG

% transform numeric file name into string
if isnumeric(fn)
    fn = num2str(fn);
end

%% generate folders after check of existence

% ID folder
if ~exist([pn '/' fn],'dir')
    mkdir([pn '/' fn '/']);
    display(['generated ' pn '/' fn '/'])
else
    warning(['folder ' pn '/' fn '/  already exists - not overwritten'])   
end

% anatomy data folder
if ~exist([pn '/' fn '/anatomy/'],'dir')
    mkdir([pn '/' fn '/anatomy/']);
    display(['generated ' pn '/' fn '/anatomy/'])
else
    warning(['folder ' pn '/' fn '/anatomy/  already exists - not overwritten'])   
end

% behavioral data folder
if ~exist([pn '/' fn '/beh/'],'dir')
    mkdir([pn '/' fn '/beh/']);
    display(['generated ' pn '/' fn '/beh/'])
else
    warning(['folder ' pn '/' fn '/beh/  already exists - not overwritten'])   
end

% behavioral raw data folder
if ~exist([pn '/' fn '/raw_beh/'],'dir')
    mkdir([pn '/' fn '/raw_beh/']);
    display(['generated ' pn '/' fn '/raw_beh/'])
else
    warning(['folder ' pn '/' fn '/raw_beh/  already exists - not overwritten'])   
end

% EEG data folder
if ~exist([pn '/' fn '/eeg/'],'dir')
    mkdir([pn '/' fn '/eeg/']);
    display(['generated ' pn '/' fn '/eeg/'])
else
    warning(['folder ' pn '/' fn '/eeg/  already exists - not overwritten'])   
end

% EEG raw data folder
if ~exist([pn '/' fn '/raw_eeg/'],'dir')
    mkdir([pn '/' fn '/raw_eeg/']);
    display(['generated ' pn '/' fn '/raw_eeg/'])
else
    warning(['folder ' pn '/' fn '/raw_eeg/  already exists - not overwritten'])   
end

% history / documentation folder
if ~exist([pn '/' fn '/history/'],'dir')
    mkdir([pn '/' fn '/history/']);
    display(['generated ' pn '/' fn '/history/'])
else
    warning(['folder ' pn '/' fn '/history/  already exists - not overwritten'])   
end

% comments .txt file
if ~exist([pn '/' fn '/comments.txt'],'file')
    tmp = [];
    save([pn '/' fn '/comments.txt'],'tmp','-ascii')
    display(['generated ' pn '/' fn '/comments.txt'])
else
    warning(['file ' pn '/' fn '/comments.txt  already exists - not overwritten'])   
end


