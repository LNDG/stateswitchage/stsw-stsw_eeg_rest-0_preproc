STSWD Rest Notes

ss_118 manually renamed to ss_1118
1247_rest manually renamed to ss_1247_rest
1163_rest manually renamed to ss_1163_rest

2201 - no rest data available
1213 - dropped as channel locations couldn't easily be inferred (Remember that we recorded the data with wrong locations. For this initial subject the setup deviated from both the pilot and study setup and I found it hard to reconstruct. If someone is very eager, he/she may try to further play around with the channel locations to get sensible autocorrelations.)

IDs = {'1163'; '1167'; '1216'; '1245'; '1247'} had to be reimported to EEGlab and FieldTrip

ICA Notes

% Channel interpolation prior to ICA happens for:
% 1120: F4
% 2121: PO10